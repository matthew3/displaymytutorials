<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('php-scripts/functions/database.php');
	require_once('php-scripts/classes/BatchQuery.php');

	function addError($label, $str)
	{
		if (!isset($_SESSION['new_section_errors']))
		{
			$_SESSION['new_section_errors'] = array();
		}
		$_SESSION['new_section_errors'][] = $str;
	}
	
	function redirectIfErrors($databaseLink)
	{
		if (isset($_SESSION['temp']))
		{
			$_SESSION['new_section_errors'] = array();
			$_SESSION['new_section_errors'] = $_SESSION['temp'];
			unset($_SESSION['temp']);
			$databaseLink->close();
			header('Location: ' . $_SERVER['HTTP_REFERER']);
		}
	}
	
	//ensure user is logged in
	include('php-scripts/functions/restriction.php');
	ensure_user_login(true, 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
	
	$tutID = 0;
	if (isset($_GET['id']))
	{
		$tutID = $_GET['id'];
	}
	else
	{
		addError('no_id', 'No id was specified.');
	}
	
	redirectIfErrors(null);
	
	$link = openDatabase();
	
	//get title and author
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT title, author_id FROM tutorials WHERE id=? AND deleted=0",
						'i', array($tutID));
	$results = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve tutorial information. Please try again later.');
	}
	
	unset($q);
	
	$realAuthor = $results[0]['author_id'];
	$tutTitle = $results[0]['title'];
	
	$IS_AUTHOR = false;
	
	if ($realAuthor == $_SESSION['user_id'])
	{
		$IS_AUTHOR = true;
	}
	else
	{
		header('Location: http://' . $_SERVER['SERVER_NAME'] . '/restricted_area.php');
		exit();
	}
	
	/**Find the number of sections this tutorial has**/
	
	$num_sections = 0;
	
	/**Retrieve all tutorial sections for this tutorial**/
	/*
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT sequence_num, title FROM sections WHERE tut_id=? AND parent_id=0 AND deleted=0 ORDER BY sequence_num ASC",
								'i', array($tutID));
	$sections = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve tutorial information. Please try again later.');
	}
	
	unset($q);
	*/
	
	/*determine if we're making a new section or just updating*/
	$invalidGet = false;
	
	$action = '';
	$sectID = 0;
	if (isset($_GET['action']))
	{
		if ($_GET['action'] == 'new')
		{
			$action = 'php-scripts/create_section.php';
		}
		else if ($_GET['action'] == 'update')
		{
			$action = 'php-scripts/update_section.php';
			if (isset($_GET['sect_id']))
			{
				$sectID = $_GET['sect_id'];
			}
			else
			{
				$invalidGet = true;
			}
		}
	}
	else //must be new
	{
		$action = 'php-scripts/create_section.php';
		$num_sections += 1; //allow creating a section in a new sequence order
	}
	
	$sect = array('id' => $sectID,
						'title' => '',
						'html' => '',
						'sequence_num' => 1,
						'parent_id' => 0,
						'tags' => '');
	
	if ($invalidGet)
	{
		addError('invalid_get', 'The specified action does not exist. Try going back <a href="index.php">home</a>');
	}
	
	/*Get section information if we're updating a section*/
	if ($_GET['action'] == 'update')
	{
		/*get section information*/
		$q = new BatchQuery($link);
		$q->addParamQuery("SELECT id, title, html, sequence_num, parent_id, tags FROM sections WHERE id=? AND deleted=0",
							'i', array($sect['id']));
		$sect = $q->execute();
		$sect = $sect[0];
		
		if ($q->anyErrors())
		{
			addError('database','Could not retrieve tutorial information. Please try again later.');
		}
		
		unset($q);
		
		//get the total size of all image files present
		
		//search for each image file
		$pattern = "#<img([^s]+)?src=\"([^:\"]+)\"[^\/<]+(\/>|<\/img>)#i"; //will only match relative paths, won't include linked images
		$success = preg_match_all($pattern, html_entity_decode($sect['html']), $matches, PREG_SET_ORDER);
		
		if ($success == false)
		{
		}
		else if ($success > 0)
		{
			//add its size to the accumulation
			$_SESSION['uploads_size'] = 0;
			
			$count = count($matches);
			for ($match_num = 0; $match_num < $count; $match_num++)
			{
				$path = $matches[$match_num][1];
				if (file_exists($path))
				{
					$_SESSION['uploads_size'] += intval(filesize($path) / 1024); //turn filesize into kilobytes
				}
			}
		}
	}
	else //new section
	{
		//there aren't any images yet
		$_SESSION['uploads_size'] = 0;
	}
	
	/*Get only the top level sections to allow inclusion as a parent section*/
	$parentSects = array();
	
	$sectionID = (isset($sect['id'])? $sect['id'] : 0);
	
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT id, title, sequence_num FROM sections WHERE tut_id=? AND parent_id=0 AND deleted=0 ORDER BY sequence_num ASC",
						'i', array($tutID));
	$parentSects = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve tutorial information. Please try again later.');
	}
	
	unset($q);
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="http://www.displaymy.com/css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/new_section.css" />
		
		<link rel="stylesheet" type="text/css" href="modules/css/top-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/lower-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/bottom-banner.css" />
	
		<link rel="stylesheet" type="text/css" href="modules/control-panel/css/control-panel.css" />
		
		<link rel="icon" href="resources/favicon.ico" type="image/x-icon" sizes="16x16" />
		
		<script type="text/javascript" src="javascript/libraries/jquery-1.11.1.min.js"></script>
		
		<link rel="stylesheet" type="text/css" href="javascript/libraries/uploadify/uploadify.css" />
		
		<meta charset="utf-8"> 
		
		<title>DMT - Section Creation - <?php echo $tutTitle; ?></title>
		
		<script type="text/javascript">
			var backupName = 'backup-sect<?php echo $sect['id']; ?>';
			
			function stayLoggedIn()
			{
				//send post to have session stay logged in
				$.post("php-scripts/stay_logged_in.php", {}, function(result){});
			}
			setInterval(function(){stayLoggedIn()}, 300000); //stay logged in for every 5 minutes spent on this page.
		</script>
	</head>
	<body>
		<?php
			include('modules/top-banner.php');
		?>
		
		<script type="text/javascript">
			saved = false;
			window.onbeforeunload = function()
			{
				if (!saved)
				{
					return 'Are you sure you want to leave this page?';
				}
			};
		</script>
		
		<div class="middle-banner">
			<div class="main_full">
				<?php
				//If we have errors on this page, output them then clear them
					if (isset($_SESSION['new_section_errors']))
					{
				?>
						<div class="errors">
							<ul>
						<?php
								foreach ($_SESSION['new_section_errors'] as $errorStr)
								{
									echo '<li>' . $errorStr . '</li>';
								}
						?>
							</ul>
						</div>
				<?php
						unset($_SESSION['new_section_errors']); //clear the errors
					}
				?>
				<h1>
					<?php echo (($_GET['action'] == 'update')? 'Updating section ' : 'New section for '); ?>&quot;
					<?php echo $tutTitle; ?>&quot;: <?php echo $sect['title']; ?>
				</h1>
				
				<div class="new_section">
					<form id="create_section_form" action="<?php echo $action; ?>" method="post" onsubmit="saved = true; return set_content(backupName)" accept-charset="UTF-8">
						<input class="hidden" type="text" name="winnie" />
						<input type="hidden" name="tut_id" value="<?php echo $tutID; ?>" />
						<input type="hidden" name="sect_id" value="<?php echo $sect['id']; ?>" />
						<input type="hidden" name="prev_seq_num" value="<?php echo $sect['sequence_num']; ?>" />
						<input type="hidden" name="prev_parent_id" value="<?php echo $sect['parent_id']; ?>" />
						
						Which section is this subsection part of?
						<select id="parent_id" name="parent_id" onchange="getChildrenSections()">
							<option value="0">None (not a subsection)</option>
							<?php
								foreach ($parentSects as $parentSect)
								{
									if ($parentSect['id'] != $sect['id'])
									{
										$selected = (($parentSect['id'] == $sect['parent_id'])? 'selected' : '');
										echo '<option value="' . $parentSect['id'] . '"' . $selected . '>' . $parentSect['title'] . '</option>';
									}
								}
							?>
						</select>
						
						<br />
						
						<div id="seq_num_select_div">
							<input type="hidden" name="seq_num_id" value="0" />
						</div>
						
						<script type="text/javascript">
							function getChildrenSections()
							{
								var sel = document.getElementById('parent_id');
								var new_parent_id = sel.options[sel.selectedIndex].value;
								
								$.post("php-scripts/get_sections_of_parent.php", {id: new_parent_id,
																									sect_id: <?php echo $sect['id']; ?>,
																									tut_id: <?php echo $tutID; ?>,
																									sequence_num: <?php echo $sect['sequence_num']; ?>}, function(result)
								{
									document.getElementById('seq_num_select_div').innerHTML = result;
								});
							}
							
							getChildrenSections();
						</script>
						
						<h3>Title</h3>
						
						<input type="text" name="title" size="40" maxlength="40" value="<?php echo $sect['title']; ?>" />
						<br />
						<input type="hidden" id="editor_num_field" name="editor_num_field" value="1" />
						<input type="hidden" id="content_field" name="content_field" value="" />
						
						<script type="text/javascript">
							function preview_backup(backupName)
							{
								var backupContent = localStorage.getItem(backupName);
								
								if (backupContent != null)
								{
									document.getElementById('preview').innerHTML = "<div class=\"reg-button\" onclick=\"document.getElementById('preview').style.display = 'none'\">Close</div><br />" + backupContent;
									document.getElementById('preview').style.display = 'block';
									document.getElementById('preview').style.top = '330px';
								}
								else
								{
									alert("No backup yet.");
								}
							}
							
							function preview_content()
							{
								document.getElementById('preview').innerHTML = "<div class=\"reg-button\" onclick=\"document.getElementById('preview').style.display = 'none'\">Close</div><br />";
								document.getElementById('preview').innerHTML += get_current_editor_contents();
								document.getElementById('preview').style.display = 'block';
								document.getElementById('preview').style.top = '700px';
							}
							
							function save_to_backup(backupName)
							{
								var ans = confirm('Your current backup will be replaced by the content that is in your editor. Continue?');
								
								if (!ans)
								{
									return false;
								}
								
								var tmp = get_current_editor_contents();
								localStorage.setItem(backupName, tmp);
								
								 //this line is to fix chrome bug where persistence requires a read (https://code.google.com/p/chromium/issues/detail?id=160056)
								localStorage.getItem(backupName);
								
								return true;
							}
						
							function handle_backup(backupName)
							{
								var ans = confirm('The content you current have in your editor will become backed up, and this backup will become your content. Continue?');
								
								if (!ans)
								{
									return false;
								}
								
								//trade backup and editor contents
								var tmp = get_current_editor_contents();
								set_to_backup(backupName);
								localStorage.setItem(backupName, tmp);
								localStorage.getItem(backupName); //chrome bug
								return true;
							}
							
							function set_to_backup(backupName)
							{
								var backupContent = localStorage.getItem(backupName);
								
								if (backupContent != null)
								{
									if (confirm("The content will be replaced by: " + backupContent + ". Continue?"))
									{
										document.getElementById('regular_editor').value = backupContent; //set regular to backup content
										tinyMCE.EditorManager.get('formatted_editor').setContent( backupContent ); //set formatted to backup content
										e.setValue(backupContent, 1); //set code to backup content
									}
								}
								else
								{
									alert("No backup yet.");
								}
							}
						</script>
						
						<div id="preview"></div>
							
						<div>
							<h3>Backup</h3>
							<div class="reg-button" style="width:120px;" onclick="save_to_backup(backupName)">Save Backup</div>
							<div class="reg-button" style="width:120px;" onclick="preview_backup(backupName)">Preview Backup</div>
							<div class="reg-button" onclick="handle_backup(backupName)">Use Backup</div>
						</div>
						<br />
						<div>
							<div class="editor_change_btn" onclick="regular_editor()">Plain Text</div>
							<div class="editor_change_btn" onclick="formatted_editor()">Formatted Text</div>
							<div class="editor_change_btn" onclick="code_editor()">Code</div>
						</div>
						
						<!-- requires this for some reason, otherwise consumes above div (some issue in tinyMCE) -->
						<div>
							<br />
							<br />
						</div>
						
						<div id="content_editor">
							<div id="regular_editor_span">
								<textarea id="regular_editor" class="regular_editor" name="regular_content"><?php echo html_entity_decode($sect['html']); ?></textarea>
							</div>
							
							<div id="formatted_editor_span" style="visibility:hidden; display:none;">
								
								<input id="file_upload" name="Filedata" type="file" multiple="true"> <!-- for uploadify -->
								<textarea id="formatted_editor" class="formatted_editor"></textarea>
								
								<script type="text/javascript" src="/javascript/libraries/tinymce/tinymce.min.js"></script>
								<script type="text/javascript">
								tinyMCE.init({
									mode: "specific_textareas",
									editor_selector: "formatted_editor",
									plugins: [
										"autolink image link lists advlist preview anchor",
										"table, media, insertdatetime, paste"
									],
									toolbar: [
										"insertfile undo redo | sizeselect styleselect | fontselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
									],
									height: 500
								 });
								</script>
								
								<script type="text/javascript" src="/javascript/libraries/uploadify/jquery.uploadify.min.js"></script>
								<script type="text/javascript">
									$(function() {
										$('#file_upload').uploadify({
											'swf'  : 'javascript/libraries/uploadify/uploadify.swf',
											'uploader'    : 'javascript/libraries/uploadify/uploadify.php',
											'fileDesc'  : 'Image Files',
											'cancelImg' : 'javascript/libraries/uploadify/uploadify-cancel.png',
											'fileExt'   : '*.jpg;*.JPG;*.jpeg;*.gif;*.png;*.PNG',
											'buttonText'  : 'Upload Image',
											'onUploadSuccess': function (file, data, response) {
													tinyMCE.EditorManager.get('formatted_editor').execCommand("mceInsertContent", false, data);
												}
											});
									});
								</script>
								
							</div>
							
							<div id="code_editor_span" style="visibility:hidden; display:none;">
								<div id="editor"></div>
								<script src="/javascript/libraries/ace/ace.js" type="text/javascript" charset="utf-8"></script>
								<script>
									var e = ace.edit("editor");
									e.setTheme("ace/theme/monokai");
									e.getSession().setMode("ace/mode/html");
								</script>
							</div>
							<script>
								var editor_num = 1;
								
								function get_current_editor_contents()
								{
									if (editor_num === 1)
									{
										return document.getElementById('regular_editor').value;
									}
									if (editor_num == 2)
									{
										return tinyMCE.EditorManager.get('formatted_editor').getContent();
									}
									else if (editor_num == 3)
									{
										return e.getSession().getValue();
									}
								}
								
								//called in onsbumit
								//if javascript is not enabled on the browser, the regular_editor's value should be used
								function set_content(backupName)
								{
									//fix \r\n issue in regular editor to replace it with html
										//disabled by default, instead require user to put <br /> specifically into text
									//document.getElementById('regular_editor').value = document.getElementById('regular_editor').value.replace(/\r\n|\n/g, "<br />");
								
									if (editor_num == 2)
									{
										document.getElementById('content_field').value = tinyMCE.EditorManager.get('formatted_editor').getContent();
									}
									else if (editor_num == 3)
									{
										document.getElementById('content_field').value = e.getSession().getValue();
									}
									
									document.getElementById('editor_num_field').value = editor_num;
									
									//set the new backup item for next time
									return save_to_backup(backupName);
								}
								
								function show_all()
								{
									var editor = document.getElementById('regular_editor_span');
									editor.style.visibility = 'visible';
									editor.style.display = 'inline';
									
									editor = document.getElementById('formatted_editor_span');
									editor.style.visibility = 'visible';
									editor.style.display = 'inline';
									
									editor = document.getElementById('code_editor_span');
									editor.style.visibility = 'visible';
									editor.style.display = 'inline';
								}
								
								function regular_editor()
								{
									show_all();
								
									var editor = document.getElementById('regular_editor');
									
									editor.value = get_current_editor_contents();
								
									var editor = document.getElementById('regular_editor_span');
									editor.style.visibility = 'visible';
									editor.style.display = 'inline';
									
									editor = document.getElementById('formatted_editor_span');
									editor.style.visibility = 'hidden';
									editor.style.display = 'none';
									
									editor = document.getElementById('code_editor_span');
									editor.style.visibility = 'hidden';
									editor.style.display = 'none';
									
									editor_num = 1;
									document.getElementById('editor_num_field').value = "1";
								}
								
								function formatted_editor()
								{
									show_all();
									
									var editor = document.getElementById('formatted_editor_span');
									
									tinyMCE.EditorManager.get('formatted_editor').setContent( get_current_editor_contents() );
									
									editor.style.visibility = 'visible';
									editor.style.display = 'inline';

									editor = document.getElementById('regular_editor_span');
									editor.style.visibility = 'hidden';
									editor.style.display = 'none';
									
									editor = document.getElementById('code_editor_span');
									editor.style.visibility = 'hidden';
									editor.style.display = 'none';
									
									editor_num = 2;
									document.getElementById('editor_num_field').value = "2";
								}
								
								function code_editor()
								{
									show_all();
									
									var editor = document.getElementById('code_editor_span');
									
									e.setValue( get_current_editor_contents(), 1 );
									
									editor.style.visibility = 'visible';
									editor.style.display = 'inline';
									
									editor = document.getElementById('regular_editor_span');
									editor.style.visibility = 'hidden';
									editor.style.display = 'none';
									
									editor = document.getElementById('formatted_editor_span');
									editor.style.visibility = 'hidden';
									editor.style.display = 'none';
									
									editor_num = 3;
									document.getElementById('editor_num_field').value = "3";
								}
							</script>
						</div>
						
						<br />
						
						<h3>Tags</h3>
						<span style="font-size:small;">These terms will help people search for your tutorial</span><br />
						<input type="text" maxsize="150" size="75" name="tags" value="<?php echo $sect['tags']; ?>" /> <span style="font-size:small;">separate each term with a comma (,)</span>
						
						<br />
						
						<div class="reg-button" stye="margin:5px;" onclick="preview_content()">
							Preview
						</div>
						
						<input class="reg-button" style="width:120px;" type="submit" value="Save and View" />
					</form>
				</div>
			</div>
		</div>
		
		<?php
			include('modules/bottom-banner.php');
		?>
	</body>
</html>