<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('php-scripts/functions/database.php');
	require_once('php-scripts/classes/BatchQuery.php');

	function addError($label, $str)
	{
		if (!isset($_SESSION['new_tut_errors']))
		{
			$_SESSION['new_tut_errors'] = array();
		}
		$_SESSION['new_tut_errors'][] = $str;
	}
	
	//ensure user is logged in
	include('php-scripts/functions/restriction.php');
	echo ensure_user_login(true, 'http://' . $_SERVER['SERVER_NAME'] .  $_SERVER['REQUEST_URI']);
	
	$tutID = 0;
	if (isset($_GET['id']))
	{
		$tutID = $_GET['id'];
	}
	
	$action = 'new';
	if (isset($_GET['action']))
	{
		$action = $_GET['action'];
	}
	
	$tut = array('id' => 0,
				'title' => '',
				'primary_cat_desc' => '',
				'secondary_cat_desc' => '',
				'primary_cat_id' => 0,
				'secondary_cat_id' => 0);
				
	$link = openDatabase();
	
	//if update, get tutorial's information
	if ($action == 'update' and $tutID != 0)
	{
		$q = new BatchQuery($link);
		$q->addParamQuery("SELECT id, title, image_path, primary_cat AS primary_cat_id, secondary_cat as secondary_cat_id
							FROM tutorials WHERE id=?",
							'i', array($tutID));
		$q->addParamQuery("SET @primary_id := (SELECT primary_cat FROM tutorials WHERE id=?)",
							'i', array($tutID));
		$q->addParamQuery("SET @secondary_id := (SELECT secondary_cat FROM tutorials WHERE id=?)",
							'i', array($tutID));
		$q->addQuery("SELECT description FROM categories WHERE id=@primary_id");
		$q->addQuery("SELECT description FROM categories WHERE id=@secondary_id");
		$tutorial = $q->execute();
		
		if ($q->anyErrors())
		{
			addError('database_tut', 'there was an error with the database while retrieving tutorial information.');
		}
		else
		{
			if (empty($tut))
			{
				addError('not_found', 'Sorry, the requested tutorial could not be found');
			}
			else
			{
				$tut = $tutorial[0][0];
				$tut['primary_cat_desc'] = $tutorial[3][0]['description'];
				$tut['secondary_cat_desc'] = $tutorial[4][0]['description'];
			}
		}
		
		unset($q);
	}
	
	//set the appropriate action to use
	if ($action == 'new' or $tutID == 0)
	{
		$formAction = 'php-scripts/create_tutorial.php';
	}
	else if ($action == 'update')
	{
		$formAction = 'php-scripts/update_tutorial.php';
	}
	
	/*get all categories that have a certain parent ID*/
	$q = new BatchQuery($link);
	$q->addQuery("SELECT id, description FROM categories WHERE parent_id=0");
	$categories = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database_cat', 'there was an error with the database while retrieving category information');
	}
	
	if (count($categories) == 0)
	{
		addError('database', 'there was an error with the database while retrieving category information');
	}
	
	unset($q);
	
	mysqli_close($link);
?>

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="http://www.displaymy.com/css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/new_tutorial.css" />
		
		<link rel="stylesheet" type="text/css" href="modules/css/top-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/bottom-banner.css" />
	
		<link rel="stylesheet" type="text/css" href="modules/control-panel/css/control-panel.css" />
		
		<script type="text/javascript" src="javascript/libraries/jquery-1.11.1.min.js"></script>
		
		<link rel="icon" href="resources/favicon.ico" type="image/x-icon" sizes="16x16" />
		
		<title>DMT - Tutorial Creation</title>
	</head>
	<body>
		<?php
			include('modules/top-banner.php');
		?>
		
		<div class="middle-banner">
			<div class="main_full">
				<div class="errors">
					<?php
						//If we have errors on this page, output them then clear them
						if (isset($_SESSION['new_tut_errors']))
						{
					?>
							<ul>
						<?php
							foreach ($_SESSION['new_tut_errors'] as $errorStr)
							{
								echo '<li>' . $errorStr . '</li>';
							}
						?>
							</ul>
					<?php
							unset($_SESSION['new_tut_errors']); //clear the errors
						}
					?>
				</div>
			
				<h1>
					<?php
						if ($action == 'new')
						{
							echo 'New';
						}
						else
						{
							echo 'Update';
						}
					?>
					Tutorial
				</h1>
				
				<div class="new_tutorial">
					<form id="create_tutorial_form" action="<?php echo $formAction; ?>" method="post" enctype="multipart/form-data">
						<input class="hidden" type="text" name="winnie" />
						<input class="hidden" type="text" name="id" value="<?php echo $tut['id']; ?>" />
						
						<div class="sub_heading">Title</div>
						<input type="text" name="title" size="50" maxlength="100" value="<?php echo $tut['title']; ?>" /> <br />
						
						<div class="sub_heading">Categories</div>
						<script>
							function get_new_options(catFieldName, selectElem)
							{
								var select = document.getElementById(selectElem);
								var parent = select.parentNode;
								
								//get the category id that was selected to get child categories
								var parentCatID = select.options[select.selectedIndex].value;
								
								//no options selected ("Other" was chosen)
								if (parentCatID == 0)
								{
									if (select.previousSibling != undefined && select.previousSibling.options != undefined) //if we have a sibling
									{
										//cat field's value is equal to whatever was before it
										document.getElementById(catFieldName).value = select.previousSibling.options[select.previousSibling.selectedIndex].value;
									}
									
									//remove all selects after the one that was changed
									var sib = select.nextSibling;
									while (sib != null)
									{
										parent.removeChild(sib);
										sib = select.nextSibling;
									}
									
									return;
								}
								
								//change the category value to selected
								console.log(catFieldName + ": " + parentCatID);
								document.getElementById(catFieldName).value = parentCatID;
								
								//get new sibling from php script
								if (parentCatID != 0)
								{
									$.post("php-scripts/get_new_categories.php", {parent_id: parentCatID}, function(result)
									{
										if (result != '')
										{
											//remove all selects after the one that was changed
											var sib = select.nextSibling;
											while (sib != null)
											{
												parent.removeChild(sib);
												sib = select.nextSibling;
											}
											
											var selectString = '<select class="drop-down" id="' + selectElem + '2" onchange="get_new_options(\'' + catFieldName + '\', \'' + selectElem + '2\')">';
											selectString += result;
											selectString += "</select>";
											
											$("#" + parent.id).append(selectString);
										}
									});
								}
								else
								{
									//remove all selects after the one that was changed
									var sib = select.nextSibling;
									while (sib != null)
									{
										parent.removeChild(sib);
										sib = select.nextSibling;
									}
								}
							}
						
							
							function toggle_category(toggleName, parentName, catName)
							{
								if (document.getElementById(toggleName).innerHTML == "+")
								{
									document.getElementById(toggleName).innerHTML = "-";
									document.getElementById(catName).disabled = false;
									$("#" + document.getElementById(parentName).id).show();
								}
								else
								{
									document.getElementById(toggleName).innerHTML = "+";
									$("#" + document.getElementById(parentName).id).hide();
									document.getElementById(catName).disabled = true;
								}
							}
						</script>
						
						<div id="cat1div" class="category_slot">
							<div id="parent1">
								<span style="float:left;">Primary Category: </span>
								<input type="hidden" id="cat1" name="cat1" value="<?php echo $tut['primary_cat_id']; ?>" />
								<?php
									if (!empty($tut['primary_cat_desc']))
									{
								?>
										Currently <span style="font-weight:bold;"><?php echo $tut['primary_cat_desc']; ?></span>
								<?php
									}
								?>
								<select class="drop-down" id="cat1select" onchange="get_new_options('cat1', 'cat1select')">
									<option value="0">---</option>
									<?php
										foreach ($categories as $category)
										{
											echo "<option value=\"" . $category['id'] . "\">" . $category['description'] . "</option>";
										}
									?>
								</select>
							</div>
						</div>
						
						<div id="cat2div" class="category_slot">
							<div class="toggle_btn" id="toggle2" onclick="toggle_category('toggle2', 'parent2', 'cat2')">+</div>
							<div id="parent2">
								<span style="float:left;">(optional) Secondary Category: </span>
								<input type="hidden" id="cat2" name="cat2" value="<?php echo $tut['secondary_cat_id']; ?>" />
								<?php
									if (!empty($tut['secondary_cat_desc']))
									{
								?>
										Currently <span style="font-weight:bold;"><?php echo $tut['secondary_cat_desc']; ?></span>
								<?php
									}
								?>
								<select class="drop-down" id="cat2select" onchange="get_new_options('cat2', 'cat2select')">
									<option value="0">---</option>
									<?php
										foreach ($categories as $category)
										{
											echo "<option value=\"" . $category['id'] . "\">" . $category['description'] . "</option>";
										}
									?>
								</select>
							</div>
						</div>
						
						<script>
							$("#" + document.getElementById('parent2').id).hide();
						</script>
						<br />
						
						<?php
							if ($action == 'update')
							{
						?>
								<div>
									<img src="<?php echo $tut['image_path']; ?>" width="128" height="128" />
								</div>
						<?php
							}
						?>
						<label for="file"><b>Cover Image:</b></label>
						<input type="file" name="file" id="file" /> <br />
						<br />
						
						<input class="reg-button" style="width:120px;" type="submit" value="<?php echo (($action == 'new')? 'Create Tutorial' : 'Update Tutorial'); ?>" />
					</form>
					<br />
				</div>
				
			</div>
		</div>
		
		<?php
			include('modules/bottom-banner.php');
		?>
	</body>
</html>
