<div class="sign-in">
	<script type="text/javascript">
		function userClick()
		{
			var textBox = document.getElementById('user');
			textBox.value = "";
			textBox.className = "real";
		}
		
		function passClick()
		{
			var textBox = document.getElementById('pass');
			textBox.type = "password"
			textBox.value = "";
			textBox.className = "real";
		}
		
		function userBlur()
		{
			var textBox = document.getElementById('user');
			if (textBox.value === "")
			{
				textBox.value = "Username";
				textBox.className = "suggested";
			}
		}
		
		function passBlur()
		{
			var textBox = document.getElementById('pass');
			if (textBox.value === "")
			{
				textBox.type = "text";
				textBox.value = "Password";
				textBox.className = "suggested";
			}
		}
		
		function rememberMeToggle()
		{
			var rememberElem = document.getElementById('remember');
			if (rememberElem.checked)
			{
				var str = document.getElementById('user').value;
				localStorage.setItem('username', str);
				localStorage.getItem('username'); //chrome bug, this line persists item
			}
			else
			{
				localStorage.removeItem('username');
			}
		}
	</script>
	
	<a class="panel-button" style="border-right:2px solid #435348;" href="category.php?cat_id=0">
		Categories
	</a>
	<form action="http://www.displaymy.com/php-scripts/login.php" method="post" onsubmit="rememberMeToggle()">
		<input class="suggested" type="text" maxlength="20" size="12" id="user" name="user_email" value="Username" onfocus="userClick()" onblur="userBlur()" />
		<input class="suggested" type="text" maxlength="20" size="12" id="pass" name="pass" value="Password" onfocus="passClick()" onblur="passBlur()" />
			
		<input type="submit" value="Log In" /> 	
		<a href="http://www.displaymy.com/registration.php">Register</a>
		
		<div style="float:right;">
			<!-- This link must stay as NO SUBDOMAIN because hybridauth will break if it has "www."-->
			<a href="http://displaymy.com/php-scripts/social_login.php?s=Facebook" style="text-decoration:none; color:black;"
				onclick="document.getElementById('fb_img').style.display='none';
								document.getElementById('load_img').style.display='';">
				<img id="fb_img" src="resources/images/facebook_login_small.png" width="81" height="40" />
				<img id='load_img' style="display:none;" src="resources/images/facebook_loading.gif" width="81" height="40" />
			</a>
		</div>
		
		<br />
		<!--
		<span style="font-size:12px;">
			<input id="remember" type="checkbox" name="remember" /> Remember Me
		</span>
		-->
	</form>
	
	<script type="text/javascript">
		var username = localStorage.getItem('username');
		if (username)
		{
			document.getElementById('user').value = username;
		}
	</script>
	
</div>