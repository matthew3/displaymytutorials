<div class="top-banner-background">
	<div class="top-banner">
		<a class="top-left" href="index.php">
			<span class="display-font">Display</span>
			<span class="my-font">MY</span>
			<span class="niche-font">tutorials</span>
		</a>
		
		<div class="top-middle">
			<div class="search-area">
				<form action="search.php" method="get">
					<select name="t">
						<option value="tutorials" <?php if (isset($_GET['t']) and $_GET['t'] == 'tutorials') echo 'selected'; ?>>Tutorials</option>
						<option value="sections" <?php if (isset($_GET['t']) and $_GET['t'] == 'sections') echo 'selected'; ?>>Sections</option>
					</select>
					<input type="text" maxlength="50" size="20" name="s" value="<?php if (isset($_GET['s'])) echo $_GET['s']; ?>" />
					<input type="submit" value="Search" />
				</form>
			</div>
		</div>
		
		<?php
			if (isset($_SESSION['logged_in']) and $_SESSION['logged_in'])
			{
				include($_SERVER['DOCUMENT_ROOT'] . "/modules/control-panel/control-panel.php");
			}
			else
			{
				include($_SERVER['DOCUMENT_ROOT'] . "/modules/control-panel/sign-in.php");
			}
		?>
	</div>
</div>
<div class="top-spacer">
</div>
<div style="width:100%;">
	<div class="call-to-action">
		<?php
			if (isset($_SESSION['logged_in']) and $_SESSION['logged_in'])
			{
				echo '<a href="new_tutorial.php?action=new" class="reg-button" style="width:160px;">Share My Knowledge</a>';
				echo ' or ';
				echo '<a href="http://tutorials.displaymy.com/tutorial.php?id=18&sect=24" class="reg-button" style="width:120px;" target="_blank">Learn How</a>';
			}
			else
			{
				echo 'Help share your knowledge! <a href="http://www.displaymy.com/registration.php" class="reg-button" style="width:160px;">Get My Free Account</a>';
				echo ' or ';
				echo '<a href="http://tutorials.displaymy.com/tutorial.php?id=18" class="reg-button" style="width:120px;" target="_blank">Learn How</a>';
			}
		?>
	</div>
</div>
