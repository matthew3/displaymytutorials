<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" type="text/css" href="http://www.displaymy.com/css/main.css" />
		
		<link rel="stylesheet" type="text/css" href="modules/css/top-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/lower-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/bottom-banner.css" />
	
		<link rel="stylesheet" type="text/css" href="modules/control-panel/css/control-panel.css" />
		
		<link rel="icon" href="resources/favicon.ico" type="image/x-icon" sizes="16x16" />
		
		<title>Display My Tutorials - Teach and Be Taught</title>
		
		<!-- general meta (google) -->
		<meta name="description" content="Learn a concept to up your mindset! Display My Tutorials to learn and help others master any topic now." />
		<meta http-equiv='Content-Type' content='Type=text/html; charset=utf-8' />
		
		<!-- open graph meta (facebook) -->
		<meta property="og:title" content="Display My Tutorials - Teach and Be Taught" />
		<meta property="og:type" content="website" />
		<meta property="og:image" content="<?php echo $_SERVER['DOCUMENT_ROOT'] ?>/resources/images/default.jpg" />
		<meta property="og:url" content="http://www.displaymy.com/index.php" />
		<meta property="og:description" content="Learn a concept to up your mindset! Display My Tutorials to learn and help others master any topic now." />
		
		<!-- twitter meta -->
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:url" content="http://www.displaymy.com/index.php" />
		<meta name="twitter:title" content="Display My Tutorials - Teach and Be Taught" />
		<meta name="twitter:description" content="Learn a concept to up your mindset! Display My Tutorials to learn and help others master any topic now." />
		<meta name="twitter:image" content="<?php echo $_SERVER['DOCUMENT_ROOT'] ?>/resources/images/default.jpg" />
	</head>
	<body>
		<?php
			include('modules/top-banner.php');
		?>
		
		<div class="middle-banner">	
			<div class="main_full">
				<div style="padding:10px;">
					<?php
						if (!isset($_GET['msg']))
						{ 
							if (isset($_GET['err']))
							{
					?>
								<div class="errors">
									<?php echo $_GET['err']; ?>
								</div>
					<?php
							}
						}
						else
						{
					?>
							<div class="success">
								<?php echo $_GET['msg']; ?>
							</div>
					<?php
						}
					?>
				
					<h1>Welcome to DisplayMyTutorials (DMT)</h1>
					<h3>What are we here for?</h3>
					<p>
						This is a new website geared towards open knowledge.
						If you know something and want to share it, <a href="new_tutorial.php">make a tutorial</a> for it!
						If you came here looking for a tutorial on a specific topic, use our search bar to find that topic by entering keywords.
						Are you just bored and want to learn something random? Use your random button in the bottom left corner.
					</p>
					<h3></h3>
				</div>
			</div>
		</div>
		
		<?php
			include('modules/bottom-banner.php');
		?>
	</body>
</html>