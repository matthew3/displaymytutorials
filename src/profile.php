<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('php-scripts/functions/database.php');
	require_once('php-scripts/classes/BatchQuery.php');

	include('php-scripts/functions/constants.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['account_errors']))
		{
			$_SESSION['account_errors'] = array();
		}
		$_SESSION['account_errors'][] = $str;
	}
	
	if (!(isset($_GET['id']) or isset($_GET['user'])))
	{
		header('http://' . $_SERVER['SERVER_NAME'] . '/index.php?err=This%20user%20does%20not%20exist.');
		exit;
	}
	
	$userID = 0;
	if (isset($_GET['id']))
	{
		$userID = $_GET['id'];
	}
	
	$userName = '';
	if (isset($_GET['user']))
	{
		$userName = $_GET['user'];
	}
	
	/**Retrieve all tutorials created by this user, don't worry about the tutorial's parts for now**/
	$link = openDatabase();
	
	//get tutorial information for current user
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT user FROM displaymy_db.users WHERE id=? OR user=? LIMIT 1",
						'is', array($userID, $userName));
	$user = $q->execute();
	
	if (count($user) > 0)
	{
		$user = $user[0];
	}
	else
	{
		addError('query','Could not retrieve account information. Please try again later.');
	}
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve account information. Please try again later.');
	}
	
	unset($q);
	
	$tutorials = array();
	if ($userID != 0)
	{
		//get tutorial information for all tutorials
		$q = new BatchQuery($link);
		$q->addParamQuery("SELECT id, title, image_path FROM tutorials WHERE author_id=? AND deleted=0",
							'i', array($userID));
		$tutorials = $q->execute();
		
		if ($q->anyErrors())
		{
			addError('database','Could not retrieve tutorial information. Please try again later.');
		}
	}
	
	unset($q);
	
	mysqli_close($link);
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="http://www.displaymy.com/css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/account.css" />
		<link rel="stylesheet" type="text/css" href="css/tutorial.css" />
		
		<link rel="stylesheet" type="text/css" href="modules/css/top-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/lower-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/bottom-banner.css" />
	
		<link rel="stylesheet" type="text/css" href="modules/control-panel/css/control-panel.css" />
		
		<link rel="icon" href="resources/favicon.ico" type="image/x-icon" sizes="16x16" />
		
		<title>DMT - <?php echo $user['user']; ?>&rsquo;s Account</title>
	</head>
	<body>
		<?php
			include('modules/top-banner.php');
		?>
		
		<div class="middle-banner">
			<div class="main_full">
				<div class="errors">
					<?php
						//If we have errors on this page, output them then clear them
						if (isset($_SESSION['account_errors']))
						{
					?>
							<ul>
						<?php
							foreach ($_SESSION['account_errors'] as $errorStr)
							{
								echo '<li>' . $errorStr . '</li>';
							}
						?>
							</ul>
					<?php
							unset($_SESSION['account_errors']); //clear the errors
						}
					?>
				</div>
			
				<h1>
					<?php echo $user['user']; ?>&rsquo;s Page
				</h1>
				
				<div class="profile-info">
					<div class="profile-pic">
						<img src="" width="128px" height="128px" />
					</div>
					
					<div class="info">
						<h3>Information</h3>
						<table>
							<tr>
								<td>Username:</td>
								<td><?php echo $user['user']; ?></td>
							</tr>
						</table>
					</div>
				</div>
				
				<div class="user-tutorials">
					<h3><?php echo $user['user'] ?>&rsquo;s Tutorials</h3>
					<?php
						foreach ($tutorials as $tut)
						{
							$image_path = '';
							
							if (file_exists($tut['image_path']))
							{
								$image_path = $tut['image_path'];
							}
							else
							{
								$image_path = DEFAULT_IMAGE_PATH();
							}
					?>
						<div class="tutorial" onclick="window.location='tutorial.php?id=<?php echo $tut['id']; ?>'">
							<div class="tutorial-image">
								<img style="float:left;" src="<?php echo $image_path; ?>" />
							</div>
							
							<div class="tutorial-info alpha60">
								<div class="tutorial-title">
									<a class="link-title" href="tutorial.php?id=<?php echo $tut['id']; ?>">
										<?php echo $tut['title']; ?>
									</a>
								</div>
							</div>
						</div>
					<?php
						}
					?>
				</div>
			</div>
		</div>
		
		<?php
			include('modules/bottom-banner.php');
		?>
	</body>
</html>