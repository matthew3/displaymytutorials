<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('php-scripts/functions/database.php');
	include('php-scripts/classes/BatchQuery.php');
	
	include('php-scripts/functions/constants.php');
	
	$searchText = '';
	if (isset($_GET['s']))
	{
		$searchText = $_GET['s'];
	}
	
	$searchType = 'sections';
	if (isset($_GET['t']))
	{
		$searchType = $_GET['t'];
	}
	
	//prepare search text for split
	$searchText = preg_replace("#[^a-zA-Z0-9]+#i", " ", $searchText); //get rid of anything that is not a simple character or number
	$searchText = preg_replace("#( |,|^)[a-zA-Z0-9]{1,2}( |,|$)#i", " ", $searchText); //keep only relevant words (four letters or more)
	$searchText = preg_replace("#[ ]+#i", " ", $searchText); //keep only one space if there are multiple spaces
		
	//split keywords
	$wordPattern = '#\\s+#i';
	$keywords = preg_split($wordPattern, $searchText);
	
	$queryCondition = '([^, ]*';
	$queryCondition .= implode('[^, ]*)|([^, ]*', $keywords);
	$queryCondition .= '[^, ]*)';
	
	$link = openDatabase();
	
	/*Retrieve matches for the keywords*/
	$q = new BatchQuery($link);

	if ($searchType == 'sections')
	{
		$q->addParamQuery("SELECT s.id AS id, s.title AS title, s.tut_id AS tut_id, s.creation_date AS creation_date,
											u.id AS author_id, u.user AS author, t.image_path AS image_path
											FROM sections AS s, users AS u, tutorials AS t
											WHERE s.deleted=0 AND u.id=s.author_id AND t.id=s.tut_id AND ((s.tags REGEXP ?) OR (s.title REGEXP ?))",
							'ss', array($queryCondition, $queryCondition));
	}
	else if ($searchType == 'tutorials')
	{
		$q->addParamQuery("SELECT t.id AS id, t.title AS title, t.image_path AS image_path, t.creation_date AS creation_date,
											u.id AS author_id, u.user AS author
											FROM tutorials AS t, users AS u
											WHERE t.deleted=0 AND u.id=t.author_id AND (t.title REGEXP ?)",
							's', array($queryCondition));
	}
	
	$sectionResults = $q->execute();
	
	if ($q->anyErrors())
	{
		$sectionResults = array();
		echo $q->getErrors()[0];
	}
	
	mysqli_close($link);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" type="text/css" href="http://www.displaymy.com/css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/tutorial.css" />
		
		<link rel="stylesheet" type="text/css" href="modules/css/top-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/lower-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/bottom-banner.css" />
	
		<link rel="stylesheet" type="text/css" href="modules/control-panel/css/control-panel.css" />
		
		<link rel="icon" href="resources/favicon.ico" type="image/x-icon" sizes="16x16" />
		
		<title>DMT - Search - <?php echo $searchText; ?></title>
	</head>
	<body>
		<?php
			include('modules/top-banner.php');
		?>
		
		<div class="middle-banner">	
			<div class="main_full">
				<h1>Search Results</h1>
				<div>
					<?php
						if ($searchType == 'sections')
						{
							foreach ($sectionResults as $sect)
							{
					?>
								<div class="tutorial" onclick="window.location='tutorial.php?id=<?php echo $sect['tut_id']; ?>&sect=<?php echo $sect['id']; ?>'">
									<div class="tutorial-image">
										<img style="float:left;" src="<?php echo $sect['image_path']; ?>" />
									</div>
									<div class="tutorial-info alpha60">
										<div id="link<?php echo $sect['id']; ?>" class="tutorial-title">
											<a id="title<?php echo $sect['id']?>>"href="tutorial.php?id=<?php echo $sect['tut_id']; ?>&sect=<?php echo $sect['id']; ?>">
												<?php echo $sect['title']; ?>
											</a>
										</div>
										<script>document.getElementById('link<?php echo $sect['id']?>').innerHTML = '<?php echo $sect['title']; ?>';</script>
										<div class="tutorial-author">
											<span style="font-weight:bold;">by</span> <a href="profile.php?id=<?php echo $sect['author_id']; ?>"><?php echo $sect['author']; ?></a>
										</div>
									</div>
								</div>
					<?php
							}
						}
						else if ($searchType == 'tutorials')
						{
							foreach ($sectionResults as $t)
							{
								$image_path = '';
							
								if (file_exists($t['image_path']))
								{
									$image_path = $t['image_path'];
								}
								else
								{
									$image_path = DEFAULT_IMAGE_PATH();
								}
					?>
								<div class="tutorial" onclick="window.location='tutorial.php?id=<?php echo $t['id']; ?>'">
									<div class="tutorial-image">
										<img style="float:left;" src="<?php echo $image_path; ?>" width="128" height="128" />
									</div>
									<div class="tutorial-info alpha60">
										<div id="link<?php echo $t['id']; ?>" class="tutorial-title">
											<a href="tutorial.php?id=<?php echo $t['id']; ?>">
												<?php echo $t['title']; ?>
											</a>
										</div>
										<script>document.getElementById('link<?php echo $t['id']?>').innerHTML = '<?php echo $t['title']; ?>';</script>
										<div class="tutorial-author">
											<span style="font-weight:bold;">by</span> <a href="profile.php?id=<?php echo $t['author_id']; ?>"><?php echo $t['author']; ?></a>
										</div>
									</div>
								</div>
					<?php
							}
						}
						
						if (count($sectionResults) == 0)
						{
							echo "No results were found, please try using different keywords.";
						}
					?>
				</div>
			</div>
		</div>
		
		<?php
			include('modules/bottom-banner.php');
		?>
	</body>
</html>