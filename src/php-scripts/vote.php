<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	//ensure user is logged in
	include('functions/restriction.php');
	if (!ensure_user_login(false))
	{
		echo 'Please <a href="http://www.displaymy.com/request_login.php" target="_blank">login</a> or <a href="registration.php" target="_blank">register</a> to use this feature.';
		exit();
	}
	
	$sectID = $_POST['section_id'];
	$voteType = $_POST['vote_type'];
	
	$link = openDatabase();
	
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT user_id, vote_type FROM votes WHERE user_id=? AND section_id=?",
						'ii', array($_SESSION['user_id'], $sectID));		
	$results = $q->execute();
	
	if ($q->anyErrors())
	{
		mysqli_close($link);
		unset($q);
		echo 'Sorry, an error occurred.';
		exit();
	}
	
	unset($q);
	
	$link->autocommit(false);
	
	//if user has already voted, exit
	if (count($results) > 0)
	{
		$prevVoteType = $results[0]['vote_type'];
	
		if ($prevVoteType == $voteType)
		{
			mysqli_close($link);
			unset($q);
			echo 'false';
			exit();
		}
	
		$q = new BatchQuery($link);
		
		$q->addParamQuery("UPDATE votes SET vote_type WHERE user_id=? AND section_id=?",
							'ii', array($_SESSION['user_id'], $sectID));
							
		//take away previous vote
		if ($prevVoteType == 1)
		{
			$q->addParamQuery("UPDATE sections SET up_count=up_count-1 WHERE id=?",
							'i', array($sectID));
		}
		else
		{
			$q->addParamQuery("UPDATE sections SET down_count=down_count-1 WHERE id=?",
							'i', array($sectID));
		}
		
		//perform new vote
		if ($voteType == 1)
		{
			$q->addParamQuery("UPDATE sections SET up_count=up_count+1 WHERE id=?",
								'i', array($sectID));
		}
		else
		{
			$q->addParamQuery("UPDATE sections SET down_count=down_count+1 WHERE id=?",
								'i', array($sectID));
		}
							
		$q->execute();
		
		if ($q->anyErrors())
		{
			mysqli_close($link);
			unset($q);
			echo 'false';
			exit();
		}
		
		unset($q);
		
		return 'true';
	}
	
	/*Insert a vote and count it*/
	$q = new BatchQuery($link);
	$q->addParamQuery("INSERT INTO votes VALUES (?,?,?)",
						'iii', array($_SESSION['user_id'], $sectID, $voteType));
	if ($voteType == 1)
	{
		$q->addParamQuery("UPDATE sections SET up_count=up_count+1 WHERE id=?",
							'i', array($sectID));
	}
	else
	{
		$q->addParamQuery("UPDATE sections SET down_count=down_count+1 WHERE id=?",
							'i', array($sectID));
	}
	$q->addQuery("COMMIT");
	
	$results = $q->execute();
	
	if ($q->anyErrors())
	{
		mysqli_close($link);
		unset($q);
		echo 'false';
		exit();
	}
	
	unset($q);
	
	mysqli_close($link);
	
	echo 'true';
	exit();
?>