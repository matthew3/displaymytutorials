<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	include('functions/time.php');
	
	$sectID = $_POST['sect_id'];
	$pageNum = $_POST['page_num'];
	
	if (!(isset($_POST['sect_id']) and isset($_POST['page_num'])))
	{
		echo 'Could not retrieve comments.';
		exit();
	}
	
	$startIndex = 0;
	
	if ($pageNum == 1)
	{
		$startIndex = 0;
	}
	else if ($pageNum > 1)
	{
		$startIndex = ($pageNum-1) * 10;
	}
	else
	{
		echo 'Could not retrieve comments.';
		exit();
	}
	
	$link = openDatabase();
	
	/*get all tutorials that have a certain parent ID*/
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT c.id AS id, c.comment AS comment, c.date AS date, u.user AS author, u.id AS author_id FROM comments AS c, displaymy_db.users AS u WHERE sect_id=? AND c.author_id = u.id ORDER BY date DESC LIMIT ?,?",
						'iii', array($sectID, $startIndex, 10));
	$comments = $q->execute();
	
	if ($q->anyErrors())
	{
		mysqli_close($link);
		unset($q);
		echo 'Could not retrieve comments.';
		exit();
	}
	
	unset($q);
	
	$str = '';
	foreach ($comments as $comment)
	{
		//build comments
		$str .= '<div class="comment">
						<div class="author">
							<a href="profile.php?id=' . $comment['author_id'] . '">'
							. $comment['author'] .
							'</a> <div style="float:right;">' . relativeTime(strtotime($comment['date'])) . '</div>
						</div>
						<div class="content">
							' . htmlentities($comment['comment'], ENT_QUOTES) . '
						</div>
					</div>';
	}
	
	if (count($comments) == 0)
	{
		$str = 'No comments were found for this section. Be the first to post one!';
	}
	
	//return all the new options discovered from the database
	mysqli_close($link);
	echo $str;
	exit();
?>