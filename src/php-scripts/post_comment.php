<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	include('functions/sanitization.php');
	
	//ensure user is logged in
	include('functions/restriction.php');
	if (!ensure_user_login(false))
	{	
		echo '<div class="comment">
				<div class="author">
				</div>
				<div class="content">
					Log in first <a href="request_login.php">here</a>.
				</div>
			</div>';
		exit();
	}
	
	//only allowed 1 comment every 30 seconds
	if (time() - $_SESSION['last_comment_time'] < 30)
	{
		echo '<div class="comment">
				<div class="author">
					Could not create comment.
				</div>
				<div class="content">
					Please wait a while before commenting again.
				</div>
			</div>';
		exit();
	}
	
	$link = openDatabase();
	
	if (!(isset($_POST['sect_id']) and isset($_POST['comment'])))
	{
		echo 'There was an error, try refreshing the page.';
		exit();
	}
	
	$sectID = $_POST['sect_id'];
	$comment = $_POST['comment'];
	
	//sanitize the comment. Only allow UTF-8 characters, some links and formatting tags available
	$comment = sanitizeComment($comment);
	
	if (strlen($comment) > 300)
	{
		return 'Comment was too long. Please shorten it.';
		exit;
	}
	
	//Insert new tutorial
	$q = new BatchQuery($link);
	$q->addParamQuery("INSERT INTO comments VALUES (DEFAULT(id),?,?,?,NOW())",
						'sii', array($comment, $_SESSION['user_id'], $sectID));
	$q->execute();
	
	if ($q->anyErrors())
	{
		mysqli_close($link);
		echo '<div class="comment">
				<div class="author">
					Could not create comment. Please try again later.
				</div>
				<div class="content">
					Log in first <a href="request_login.php">here</a>.
				</div>
			</div>';
		exit();
	}
	
	unset($q);
	
	mysqli_close($link);
	
	echo '<div class="comment">
			<div class="author">
				<a href="profile.php?id=' . $_SESSION['user_id'] . '">'
				. $_SESSION['user'] .
				'</a>
			</div>
			<div class="content">
				' . $comment . '
			</div>
		</div>';
	
	$_SESSION['last_comment_time'] = time();
		
	exit();
?>