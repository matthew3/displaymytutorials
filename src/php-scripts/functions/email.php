<?php

	function sendMyActivationEmail($userEmail, $activationCode)
	{		
		$send_to = $userEmail;
		
		$subject = "Welcome to DisplayMyTutorials - Account Activation";
		
		$message = "Dear User,\r\n\r\n";
		$message .= "\tWelcome to DMT, where you can teach and learn! You have requested an account at our website, and we are delighted to send you your activation code!\r\n\r\n";
		$message .= "Please enter this code to activate your account: " . $activationCode . "\r\n\r\n";
		$message .= "You can also click the link below to activate your account.\r\n\r\n";
		$message .= "http://tutorials.displaymy.com/php-scripts/activate.php?code=" . urlencode($activationCode);
		
		$headers = "From: noreply@displaymy.com";

		return mail($send_to, $subject, $message, $header, "-fnoreply@displaymy.com");
	}
	
	function sendMyPasswordReset($userID, $email, $hash)
	{
		$hash = urlencode($hash);
			
		$send_to = $email;
		
		$subject = "DisplayMy.com - Tutorials - Password Request";
		
		$message = "Dear User,\r\n\r\n";
		$message .= "\You have requested a password at tutorials.displaymy.com\r\n\r\n";
		$message .= "Please click the link below to reset your password.\r\n\r\n";
		$message .= "http://tutorials.displaymy.com/reset_password.php?u=" . $userID . "&hash=" . $hash;
		
		$headers = "From: noreply@displaymy.com";

		return mail($send_to, $subject, $message, $header, "-fnoreply@displaymy.com");

	}
?>
