<?php
	include('./libraries/sanitization/HTMLPurifier.auto.php');

	function sanitizeHTML($dirtyHTML)
	{
	
		$allowedHTMLElements = 'ol,ul,li,div,a,br,span,table,th,tr,td,h1,h2,h3,h4,h5,h6,
								code,em,strong,b,i,u,dl,dt,dd,cite,q,blockquote,p,big,small,
								sub,sup,img';
		$allowedHTMLAttributes = '*.style,a.href,th.colspan,th.rowspan,td.rowspan,td.colspan,img.src,img.width,img.height';
	
		$config = HTMLPurifier_Config::createDefault();
		
		$config->set('HTML.AllowedElements', $allowedHTMLElements);
		$config->set('HTML.AllowedAttributes', $allowedHTMLAttributes);
		$config->set('HTML.DefinitionID', 'tutorial html');
		$config->set('HTML.DefinitionRev', 1);
		$config->set('HTML.MaxImgLength', 1000);
		$config->set('HTML.FlashAllowFullScreen', true);
		
		//allow using objects in a website,
		//	possibly uncomment SafeEmbed if this doesn't work
		//$config->set('HTML.SafeEmbed', true);
		$config->set('HTML.SafeObject', true);
		$config->set('Output.FlashCompat', true);
		
		$config->set('AutoFormat.Linkify', true);
		$config->set('AutoFormat.RemoveEmpty', true);
		$config->set('AutoFormat.RemoveSpansWithoutAttributes', true);
		
		/*Use default CSS properties*/
		//$config->set('CSS.DefinitionID', 'tutorial css'); //broke the code for some reason
		$config->set('CSS.AllowTricky', false);
		$config->set('CSS.AllowedFonts', null); //all fonts allowed (serif, sans-serif, etc)
		$config->set('CSS.MaxImgLength', '700px'); //max width and height values of img attributes
		
		//Allow customer filters in order: (MIGHT NOT BE NEEDED)
		//	Allow Youtube and Vimeo,
		//$config->set('Filter.Custom', array(new HTMLPurifier_Filter_MyIframe()));
		
		$purifier = new HTMLPurifier($config);
		
		$cleanHTML = $purifier->purify($dirtyHTML);
		
		$cleanHTML = htmlspecialchars($cleanHTML, ENT_QUOTES);
		
		$cleanHTML = preg_replace('#\\&amp\\;amp\\;#i', '&amp;', $cleanHTML);
		
		return $cleanHTML;
	}
	
	function sanitizeComment($dirtyText)
	{
		$allowedHTMLElements = 'span,code,em,strong,b,i,u,big,small,sub,sup,a';
		$allowedHTMLAttributes = '*.style';
	
		$config = HTMLPurifier_Config::createDefault();
		
		$config->set('HTML.AllowedElements', $allowedHTMLElements);
		$config->set('HTML.AllowedAttributes', $allowedHTMLAttributes);
		$config->set('HTML.DefinitionID', 'tutorial html');
		$config->set('HTML.DefinitionRev', 1);
		
		$config->set('AutoFormat.Linkify', true);
		$config->set('AutoFormat.RemoveEmpty', true);
		$config->set('AutoFormat.RemoveSpansWithoutAttributes', true);
		
		/*Use default CSS properties*/
		//$config->set('CSS.DefinitionID', 'tutorial css'); //broke the code for some reason
		$config->set('CSS.AllowTricky', false);
		$config->set('CSS.AllowedFonts', null); //all fonts allowed (serif, sans-serif, etc)
		
		$purifier = new HTMLPurifier($config);
		
		$cleanText = $purifier->purify($dirtyText);
		
		return $cleanText;
	}
?>