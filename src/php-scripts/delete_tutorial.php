<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	include('functions/constants.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['temp']))
		{
			$_SESSION['temp'] = array();
		}
		$_SESSION['temp'][] = $str;
		echo $str;
	}
	
	function redirectIfErrors($databaseLink)
	{
		if (isset($_SESSION['temp']))
		{
			mysqli_close($databaseLink);
			$_SESSION['tut_management_errors'] = array();
			$_SESSION['tut_management_errors'] = $_SESSION['temp'];
			unset($_SESSION['temp']);
			
			//redirect back one page
			header("Location:" . $_SERVER['SERVER_NAME'] . "/my_tutorials.php");
		}
	}
	
	//ensure user is logged in
	include('functions/restriction.php');
	ensure_user_login(true, 'http://' . $_SERVER['SERVER_NAME'] .  '/my_tutorials.php');
	
	
	$tutID = 0;
	if (!isset($_POST['tut_id']))
	{
		addError('no_post', 'Not a valid tutorial');
	}
	else
	{
		$tutID = $_POST['tut_id'];
	}
	
	$link = openDatabase();
	
	$link->autocommit(false);
	
	$q = new BatchQuery($link);
	$q->addParamQuery("UPDATE tutorials SET deleted=1 WHERE id=? AND author_id=?",
						'ii', array($tutID, $_SESSION['user_id']));
	$q->addParamQuery("SELECT image_path FROM tutorials WHERE id=? AND author_id=? LIMIT 1",
						'ii', array($tutID, $_SESSION['user_id']));
	$q->addParamQuery("UPDATE sections SET deleted=1 WHERE tut_id=? AND author_id=?",
						'ii', array($tutID, $_SESSION['user_id']));
	$q->addQuery("COMMIT");
	
	$results = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database', 'Could not perform database operation. Please try again later.');
	}
	
	unset($q);
	
	redirectIfErrors($link);
	
	//delete the image to the tutorial
	if (!empty($results[1]))
	{
		$path = $results[1][0]['image_path']; //second query result, first row
		if (file_exists($path) and ($path != DEFAULT_IMAGE_PATH()))
		{
			unlink($path);
		}
	}
	
	mysqli_close($link);

	header("Location: http://" . $_SERVER['SERVER_NAME'] . "/my_tutorials.php");
?>