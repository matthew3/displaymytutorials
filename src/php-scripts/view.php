<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	$sectID = $_POST['section_id'];
	
	$link = openDatabase();
	
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT user_ip FROM views_temp WHERE user_ip=? AND section_id=?",
						'si', array($_SERVER['REMOTE_ADDR'], $sectID));
	$results = $q->execute();
	
	if ($q->anyErrors())
	{
		mysqli_close($link);
		unset($q);
		echo 'false';
		exit();
	}
	
	unset($q);
	
	//if user has already voted, exit
	if (count($results) > 0)
	{
		mysqli_close($link);
		echo 'true';
		exit();
	}
	
	$q = new BatchQuery($link);
	$q->addParamQuery("INSERT INTO views_temp VALUES (?,?)",
						'si', array($_SERVER['REMOTE_ADDR'], $sectID));
	$results = $q->execute();
	
	if ($q->anyErrors())
	{
		mysqli_close($link);
		unset($q);
		echo 'false';
		exit();
	}
	
	unset($q);
	
	mysqli_close($link);
	
	echo 'true';
	exit();
?>