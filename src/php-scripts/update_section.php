<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	include('functions/sanitization.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['temp']))
		{
			$_SESSION['temp'] = array();
		}
		$_SESSION['temp'][] = $str;
	}
	
	function redirectIfErrors($databaseLink, $location)
	{
		if (isset($_SESSION['temp']))
		{
			$_SESSION['new_section_errors'] = array();
			$_SESSION['new_section_errors'] = $_SESSION['temp'];
			unset($_SESSION['temp']);
							mysqli_close($databaseLink);
			header('Location: ' . $location);
			exit;
			//redirect back to tutorial_management page
		}
	}
	
	$sectID = $_POST['sect_id'];
	$tutID = $_POST['tut_id'];
	
	//ensure user is logged in
	include('functions/restriction.php');
	ensure_user_login(true, 'http://' . $_SERVER['SERVER_NAME'] .  '/new_section.php?id=' . $tutID . '&sect_id=' . $sectID . '&action=update');
	
	$link = openDatabase();
	
	$winnie = $_POST['winnie'];
	$title = $_POST['title'];
	$seqNumID = $_POST['seq_num_id'];
	$prevSeqNum = $_POST['prev_seq_num'];
	$newParentID = $_POST['parent_id'];
	$prevParentID = $_POST['prev_parent_id'];
	$tags = $_POST['tags'];
	
	$editor_num = $_POST['editor_num_field'];
	
	//get the appropriate content
	$content = '';
	if ($editor_num == 1)
	{
		$content = $_POST['regular_content']; //regular editor was active upon submission
	}
	else //javascript was enabled, so choose the other editors if they were active
	{
		$content = $_POST['content_field'];
	}
	
	//Most likely a bot (hidden field was filled in)
	if (strlen($winnie) != 0)
	{
		header('Location: catch_bot.php');
		exit;
	}
	
	if (strlen($title) < 10)
	{
		addError('title_length', 'This title is too short, please make it 10 characters or more');
	}
	else if (strlen($title) > 40)
	{
		addError('title_length2', 'This title is too long, please make it 40 characters or less');
	}
	
	//is not a positive integer
	if (!ctype_digit($seqNumID))
	{
		addError('invalid_sequence_number', 'Sorry, the sequence number was not found to be a number. Please try again');
	}
	
	redirectIfErrors($link, '../new_section.php?id=' . $tutID . '&sect_id=' . $sectID . '&action=update');
	
	$seqNum = 0;
	if ($seqNumID != 0)
	{
		//make sure seqNum is within the appropriate range
		$q = new BatchQuery($link);
		$q->addParamQuery("SELECT sequence_num FROM sections WHERE id=?",
							'i', array(intval($seqNumID)));		
		$results = $q->execute();
		
		if ($q->anyErrors())
		{
			addError('database','Could not retrieve the section. Please try again later.');
		}
		
		unset($q);
		
		if (empty($results))
		{
			addError('invalid_seq_num', 'Section part number is not in the valid range.');
		}
		
		$seqNum = $results[0]['sequence_num'] + 1;
	}
	
	//get the author and previous html
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT author_id, html FROM sections WHERE tut_id=?",
						'i', array(intval($tutID)));
	$sect = $q->execute();
	$sect = $sect[0];
	
	if ($q->anyErrors())
	{
		addError('database2','Could not retrieve the tutorial. Please try again later.');
	}
	
	unset($q);
	
	//ensure that this is the correct author
	if ($sect['author_id'] != $_SESSION['user_id'])
	{
		addError('not_author', 'You are not the author of this section.');
	}

	redirectIfErrors($link, '../new_section.php?id=' . $tutID . '&sect_id=' . $sectID . '&action=update');
	
	$title = trim($title);
	$title = sanitizeHTML($title);
	$title = htmlspecialchars($title);
	
	/*****************************************************
	******* PERFORM EXTREME INPUT SANITIZATION ***********
	*****************************************************/
	$content = trim($content);
	$cleanHTML = sanitizeHTML($content);
	$cleanHTML = htmlspecialchars($cleanHTML); //create html entities from text to be able to output HTML
	
	/*****************************************************/
	
	$link->autocommit(false);
	
	$q = new BatchQuery($link);
	
	//subtract 1 from sequence number of all that were below the updating section
	$q->addParamQuery("UPDATE sections SET sequence_num=sequence_num-1 WHERE sequence_num > ? AND parent_id=? AND tut_id=?",
						'iii', array($prevSeqNum, $prevParentID, $tutID));
	//add 1 to all that are after the updating section
	$q->addParamQuery("UPDATE sections SET sequence_num=sequence_num+1 WHERE sequence_num > ? AND parent_id=? AND tut_id=?",
						'iii', array($seqNum, $newParentID, $tutID));
	//update the section
	$q->addParamQuery("UPDATE sections SET title=?, html=?, sequence_num=?+1, tags=?, parent_id=?
						WHERE id=? AND author_id=?",
						'ssisiii', array($title, $cleanHTML, $seqNum, $tags, $newParentID, $sectID, $_SESSION['user_id']));
	$q->addQuery("COMMIT");
	
	$q->execute();
	
	if ($q->anyErrors())
	{
		addError('database', 'Could not update the section. Please try again later');
	}
	
	unset($q);
	
	redirectIfErrors($link, '../new_section.php?id=' . $tutID . '&sect_id=' . $sectID . '&action=update');
	
	mysqli_close($link);
	
	/*Succeeded in updating tutorial section*/
	
	header('Location: ../tutorial.php?id=' . $tutID . '&sect=' . $sectID);
?>