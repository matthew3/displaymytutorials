<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	include('functions/sanitization.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['temp']))
		{
			$_SESSION['temp'] = array();
		}
		$_SESSION['temp'][] = $str;
	}
	
	function redirectIfErrors($databaseLink)
	{
		if (isset($_SESSION['temp']))
		{
			$_SESSION['new_section_errors'] = array();
			$_SESSION['new_section_errors'] = $_SESSION['temp'];
			unset($_SESSION['temp']);
			mysqli_close($databaseLink);
			header('Location: ' . $_SERVER['HTTP_REFERER']);
			exit();
		}
	}
	
	//ensure user is logged in
	include('functions/restriction.php');
	ensure_user_login(true, 'http://' . $_SERVER['SERVER_NAME'] .  '/new_section.php');
	
	$link = openDatabase();
	
	$winnie = $_POST['winnie'];
	$title = $_POST['title'];
	$seqNumID = $_POST['seq_num_id'];
	$tutID = $_POST['tut_id'];
	$tags = $_POST['tags'];
	
	$parentID = $_POST['parent_id'];
	
	$editor_num = $_POST['editor_num_field'];
	
	//get the appropriate content
	$content = '';
	if ($editor_num == 1)
	{
		$content = $_POST['regular_content']; //regular editor was active upon submission
	}
	else //javascript was enabled, so choose the other editors if they were active
	{
		$content = $_POST['content_field'];
	}
	
	//Most likely a bot (hidden field was filled in)
	if (strlen($winnie) != 0)
	{
		header('Location: catch_bot.php');
		exit;
	}
	
	if (strlen($title) < 10)
	{
		addError('title_length', 'This title is too short, please make it 10 characters or more');
	}
	else if (strlen($title) > 40)
	{
		addError('title_length2', 'This title is too long, please make it 40 characters or less');
	}
	
	if (strlen($tags) > 150)
	{
		addError('tag_length', 'This amount of tags is too long, use 150 characters or less');
	}
	else if (preg_match("/[^a-zA-Z0-9, ]/i", $tags) == 1) //matches an invalid character
	{
		addError('invalid_tag', 'Only alphanumeric characters allowed between commas or spaces in the tags');
	}
	$tags = strtolower($tags);
	
	//is not a positive integer
	if (!ctype_digit($seqNumID))
	{
		addError('invalid_sequence_number', 'Sorry, the sequence number was not found to be a number. Please try again');
	}
	
	redirectIfErrors($link);
	
	//make sure seqNum is within the appropriate range
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT sequence_num FROM sections WHERE id=?",
						'i', array(intval($seqNumID)));		
	$results = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve the sequence number. Please try again later.');
	}
	
	unset($q);
	
	$seqNum = 1; //default when no section to be under was defined
	
	if (!empty($results))
	{
		$seqNum = intval($results[0]['sequence_num']) + 1;
	}
	
	redirectIfErrors($link);
	
	$title = trim($title);
	$title = sanitizeHTML($title);
	$title = htmlspecialchars($title);
	
	/*****************************************************
	******* PERFORM EXTREME INPUT SANITIZATION ***********
	*****************************************************/
	$content = trim($content);
	$cleanHTML = sanitizeHTML($content);
	$cleanHTML = htmlspecialchars($cleanHTML); //create html entities from text to be able to output HTML
	
	$link->autocommit(false);
	
	//Insert new tutorial section
	$q = new BatchQuery($link);
	$q->addParamQuery("UPDATE sections SET sequence_num=sequence_num+1 WHERE sequence_num >= ? AND parent_id=? AND tut_id=?",
						'iii', array($seqNum, $parentID, $tutID));
	$q->addParamQuery("INSERT INTO sections VALUES (DEFAULT(id),?,?,?,?,?,?,CURRENT_DATE(),NOW(),?,DEFAULT(deleted),0,0,0,0)",
						'iiissis', array($tutID, $seqNum, $_SESSION['user_id'], $title, $cleanHTML, $parentID, $tags));
	$q->execute();
	
	if ($q->anyErrors())
	{
		addError('database', 'Could not create the section. Please try again later.');
	}
	
	unset($q);
	
	redirectIfErrors($link);
	
	mysqli_commit($link); //commit here instead of SQL because of mysqli_insert_id (only works on last command, and if last command is COMMIT it won't work);
	
	$sectID = mysqli_insert_id($link); //get last inserted id
	
	mysqli_close($link);
	
	header('Location: ../tutorial.php?id=' . $tutID . "&sect=" . $sectID);
?>