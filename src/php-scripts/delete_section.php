<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['temp']))
		{
			$_SESSION['temp'] = array();
		}
		$_SESSION['temp'][] = $str;
		echo $str;
	}
	
	function redirectIfErrors($databaseLink)
	{
		if (isset($_SESSION['temp']))
		{
			if ($databaseLink)
				mysqli_close($databaseLink);
			
			$_SESSION['tut_management_errors'] = array();
			$_SESSION['tut_management_errors'] = $_SESSION['temp'];
			unset($_SESSION['temp']);
			
			//redirect back one page
			header('Location: ../my_tutorials.php', true, 302);
		}
	}
	
	if (isset($_POST['sect_id']) and isset($_POST['tut_id']))
	{
		$sectID = $_POST['sect_id'];
		$tutID = $_POST['tut_id'];
		
		//ensure user is logged in
		include('functions/restriction.php');
		ensure_user_login(true, 'http://' . $_SERVER['SERVER_NAME'] .  '/tutorial.php?id=' . $_POST['tut_id'] . '&sect=' . $_POST['sect_id']);
	}
	else
	{
		addError('no_post', 'No post variables specified.');
	}

	redirectIfErrors(null);
	
	$link = openDatabase();
	
	//set the deleted flag on the section to true
	//perform a batch process where sequence_num is also decremented if above deleted sequence_num
	$link->autocommit(false);
	
	$q = new BatchQuery($link);
	$q->addParamQuery("UPDATE sections SET deleted=1 WHERE id=? AND author_id=?",
								'ii', array($sectID, $_SESSION['user_id']));
	$q->addParamQuery("SELECT sequence_num, parent_id INTO @seq_num, @par_id FROM sections WHERE id=? LIMIT 1",
								'i', array($sectID));
	$q->addParamQuery("UPDATE sections SET sequence_num=sequence_num-1 WHERE tut_id=? AND author_id=? AND sequence_num > @seq_num AND parent_id=@par_id",
								'ii', array($tutID, $_SESSION['user_id']));
	//delete all the section's children
	$q->addParamQuery("UPDATE sections SET deleted=1 WHERE parent_id=? AND tut_id=? AND author_id=?",
								'iii', array($sectID, $tutID, $_SESSION['user_id']));
	$q->addQuery("COMMIT");
	
	$q->execute();
	
	if ($q->anyErrors())
	{
		addError('database', 'Could not perform database operation');
		echo $q->getErrors()[0];
	}
	
	unset($q);
	
	redirectIfErrors($link);
	
	mysqli_close($link);
	
	header('Location: http://' . $_SERVER['SERVER_NAME'] . '/tutorial.php?id=' . $tutID);
?>