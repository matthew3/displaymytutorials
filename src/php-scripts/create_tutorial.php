<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	include('functions/sanitization.php');
	include('functions/constants.php');
	include('functions/image.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['temp']))
		{
			$_SESSION['temp'] = array();
		}
		$_SESSION['temp'][] = $str;
	}
	
	function redirectIfErrors($databaseLink)
	{
		if (isset($_SESSION['temp']))
		{
			$_SESSION['new_tut_errors'] = array();
			$_SESSION['new_tut_errors'] = $_SESSION['temp'];
			unset($_SESSION['temp']);
			mysqli_close($databaseLink);
			header('Location: ../new_tutorial.php');
		}
	}
	
	//ensure user is logged in
	include('functions/restriction.php');
	ensure_user_login(true, 'http://' . $_SERVER['SERVER_NAME'] .  '/new_tutorial.php');
	
	$link = openDatabase();
	
	$winnie = $_POST['winnie'];
	$title = $_POST['title'];
	
	$primary_cat = 0;
	$secondary_cat = 0;
	
	if (isset($_POST['cat1']))
	{
			$primary_cat = intval($_POST['cat1']);
	}
	else
	{
		addError('prime_cat', 'Invalid primary category');
	}
	
	if ($primary_cat == 0)
	{
		addError('primary_cat', 'You must choose a category for the first one. The second one is optional');
	}
	
	if (isset($_POST['cat2']))
	{
		$secondary_cat = intval($_POST['cat2']);
	}
	
	if ($secondary_cat == $primary_cat)
	{
		$secondary_cat = 0;
	}
	
	//Most likely a bot (hidden field was filled in)
	if (strlen($winnie) != 0)
	{
		header('Location: catch_bot.php');
		exit;
	}
	
	if (strlen($title) < 10)
	{
		addError('title_length', 'This title is too short, please make it 10 characters or more');
	}
	if (strlen($title) > 100)
	{
		addError('title_length2', 'This title is too long, please make it 100 characters or less');
	}
	
	$title = sanitizeHTML($title);
	
	$savePath = '';
	
	//if an image was supplied
	if (!($_FILES['file']['error'] > 0))
	{	
		//if type is not an image
		if (!(($_FILES["file"]["type"] == "image/gif")
				|| ($_FILES["file"]["type"] == "image/jpeg")
				|| ($_FILES["file"]["type"] == "image/jpg")
				|| ($_FILES["file"]["type"] == "image/pjpeg")
				|| ($_FILES["file"]["type"] == "image/x-png")
				|| ($_FILES["file"]["type"] == "image/png")))
		{
			addError('file_type', 'sorry, the file must be an image file. Try jpeg, png, gif for best support.');
		}
		
		redirectIfErrors($link);
		
		//File is okay, save in filesystem and get link for database
		$tmp_name = $_FILES['file']['tmp_name'];
		$file_name = $_FILES['file']['name'];
		
		$saveName = $file_name;
		while (file_exists('tp/' . $saveName) and (strlen($savePath) < 256))
		{
			$rpos = strrpos($saveName, '.');
			$fileName = substr($saveName, 0, $rpos);
			$saveName = $fileName . rand(0, 9) . substr($saveName, $rpos);
		}
		$savePath = '../tp/' . $saveName;
		$tmpPath = '../tmp/' . $saveName;
		
		//move image to temporary path, create thumbnail, delete tmp;
		$success = move_uploaded_file($tmp_name, $tmpPath);
		
		//couldn't successfully save the image
		if (!$success)
		{
			addError('file_move', 'An error occurred. Renaming the file may help.');
			redirectIfErrors($link);
		}
		else
		{
			generate_image_thumbnail($tmpPath, $savePath, 128, 128);
			$success = unlink($tmpPath);
			
			if (!$success)
			{
				addError('delete_tmp', 'Could not delete the temporary file.');
			}
		}
	}
	
	redirectIfErrors($link);
	
	if (!empty($savePath))
	{
		$savePath = 'tp/' . $saveName;
	}
	else
	{
		$savePath = DEFAULT_IMAGE_PATH();
	}
	
	//get tutorial information for all tutorials
	$q = new BatchQuery($link);
	$q->addParamQuery("INSERT INTO tutorials VALUES (DEFAULT(id),?,?,?,CURRENT_DATE(),NOW(),?,?,DEFAULT(deleted),0)",
						'sisii', array($title, $_SESSION['user_id'], $savePath, $primary_cat, $secondary_cat));
	$q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not create the tutorial. Please try again later.');
	}
	
	unset($q);
	
	$tutID = mysqli_insert_id($link);
	
	redirectIfErrors($link);
	
	mysqli_close($link);
	
	header('Location: ../tutorial.php?id=' . $tutID);
?>