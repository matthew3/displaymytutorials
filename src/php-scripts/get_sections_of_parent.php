<?php
	include('functions/database.php');
	include('classes/BatchQuery.php');
	
	$link = openDatabase();

	if (!isset($_POST['id']))
	{
		echo '';
		exit;
	}
	$parentID = $_POST['id'];
	
	if (!isset($_POST['tut_id']))
	{
		echo '';
		exit;
	}
	$tutID = $_POST['tut_id'];
	
	if (!isset($_POST['sect_id']))
	{
		echo '';
		exit;
	}
	$sectID = $_POST['sect_id'];
	
	$seqNum = 0;
	if (isset($_GET['sequence_num']))
	{
		$seqNum = $_GET['sequence_num'];
	}
	
	//get all children of the current section
	$q = new BatchQuery($link);	
	$q->addParamQuery("SELECT id, title, sequence_num FROM sections WHERE parent_id=? AND tut_id=? AND deleted=0 ORDER BY sequence_num",
									'ii', array($parentID, $tutID));
	
	$children = $q->execute();
	
	if ($q->anyErrors())
	{
		echo '*An error has occurred, please wait for this to be fixed.';
		mysqli_close($link);
		exit;
	}
	
	unset($q);
	
	$selectedIndex = -1;
	if (isset($_POST['selected_index']))
	{
		$selectedIndex = $_POST['selected_index'];
	}

	echo 'Which section should this section be under?';
	echo '<select name="seq_num_id">';
	echo '<option value="0">None (very first section)</option>';
	
	foreach ($children as $child)
	{
		if ($child['id'] != $sectID)
		{
			$selected = '';
			if ($child['sequence_num'] - 1 == $seqNum)
			{
				$selected = 'selected';
			}
			
			echo '<option value="' . $child['id'] . '" ' . $selected . '>' . $child['title'] . '</option>';
		}
	}
	
	mysqli_close($link);
	
	echo '</select>';
	exit;
?>