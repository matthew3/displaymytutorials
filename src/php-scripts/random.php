<?php
	include('functions/database.php');
	include('classes/BatchQuery.php');

	function addError($label, $str)
	{
		echo $str . '<br />';
		//header('Location: ../index.php');
	}

	$link = openDatabase();
	
	//select random tutorial
	$q = new BatchQuery($link);
	$q->addQuery("SELECT id FROM tutorials WHERE deleted=0");
	$ids = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database', 'Could not retrieve tutorials, please try again later.');//'Could not update the section. Please try again later.');
	}
	
	unset($q);

	$num_tuts = count($ids);
	
	$randTut = $ids[ rand(0, $num_tuts - 1) ]['id'];
	
	//select random section
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT id FROM sections WHERE tut_id=? AND deleted=0",
							'i', array($randTut));
	$ids = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database', 'Could not retrieve sections, please try again later.');//'Could not update the section. Please try again later.');
	}
	
	unset($q);

	mysqli_close($link);
	
	$num_sects = count($ids);
	
	$randSect = '';
	if ($num_sects > 0)
	{
		$randSect = $ids[ rand(0, $num_sects - 1) ]['id'];
	}
	
	header('Location: http://' . $_SERVER['SERVER_NAME'] . '/tutorial.php?id=' . $randTut . '&sect=' . $randSect);
?>