<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('php-scripts/functions/database.php');
	include('php-scripts/classes/BatchQuery.php');

	include('php-scripts/functions/constants.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['category_errors']))
		{
			$_SESSION['category_errors'] = array();
		}
		$_SESSION['category_errors'][] = $str;
	}
	
	$catID = 0;

	if (isset($_GET['cat_id']))
	{
		$catID = $_GET['cat_id'];
	}
	
	/**Initialize variables**/
	$tutorials = array();
	$category = array();
	$categories = array();
	
	//$parents = '<a href="index.php">Home</a>';
	
	/*Get all parents of this category and add them to parents*/
	
	$link = openDatabase();
	
	/**Retrieve category information**/
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT id, description, parent_id FROM categories WHERE id=?",
						'i', array($catID));
	$category = $q->execute();
	if (count($category) > 0)
	{
		$category = $category[0];
	}
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve category information. Please try again later.');
	}
	
	unset($q);
	
	/*Retrieve all children categories of this category*/
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT id, description, parent_id FROM categories WHERE parent_id=? ORDER BY description",
						'i', array($catID));
	$categories = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve category information. Please try again later.');
	}
	
	unset($q);
	
	$tutorials = array();
	
	/**Retrieve all tutorials within chosen category**/
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT t.id AS id, title, image_path, author_id, u.user AS author FROM tutorials AS t, displaymy_db.users AS u
									WHERE ((primary_cat=? OR secondary_cat=?) OR (0=?)) AND u.id = t.author_id AND deleted=0 ORDER BY primary_cat, secondary_cat",
									'iii', array($catID, $catID, $catID));
	$tutorials = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve tutorial information. Please try again later.');
	}
	
	unset($q);
	
	mysqli_close($link);
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="http://www.displaymy.com/css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/category.css" />
		<link rel="stylesheet" type="text/css" href="css/tutorial.css" />
		
		<link rel="stylesheet" type="text/css" href="modules/css/top-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/lower-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/bottom-banner.css" />
	
		<link rel="stylesheet" type="text/css" href="modules/control-panel/css/control-panel.css" />
		
		<link rel="icon" href="resources/favicon.ico" type="image/x-icon" sizes="16x16" />
		
		<script type="text/javascript" src="javascript/libraries/jquery-1.11.1.min.js"></script>
		
		<title>DMT - Categories</title>
	</head>
	<body>
		<?php
			include('modules/top-banner.php');
		?>
		
		<div class="middle-banner">
			<div class="main_full">
			
			<?php
			//If we have errors on this page, output them then clear them
				if (isset($_SESSION['category_errors']))
				{
			?>
					<div class="errors">
						<ul>
					<?php
							foreach ($_SESSION['category_errors'] as $errorStr)
							{
								echo '<li>' . $errorStr . '</li>';
							}
					?>
						</ul>
					</div>
			<?php
					unset($_SESSION['category_errors']); //clear the errors
				}
				
				if (!empty($category))
				{
					if ($category['parent_id'] != 0)
					{
			?>
						<a href="category.php?cat_id=<?php echo $category['parent_id']; ?>">&lt;&lt; Back</a>
			<?php
					}
					else
					{
						echo '<a href="category.php">&lt;&lt; Back</a>';
					}
				}
			?>	
				<div style="width:100%; float:left;">
					<span style="float:left;">Quick Search: </span>
					<div style="float:left;" id="all-categories"></div>
				</div>
				<script type="text/javascript">
					function get_categories()
					{
						$.post("resources/all_categories.php",{}, function(result)
						{
							document.getElementById('all-categories').innerHTML = result;
						});
					}
					
					get_categories();
				</script>
				
				<div style="width:100%; float:left;">
				<?php
					if (!empty($categories))
					{
				?>
						<h1>
							Categories
						</h1>
						
						<div class="categories">
							
						<?php
							foreach ($categories as $c)
							{
						?>
								<a class="category" href="category.php?cat_id=<?php echo $c['id']; ?>"><?php echo htmlspecialchars($c['description'], ENT_QUOTES); ?></a>
						<?php
							}
						?>
							
						</div>
				<?php
					}
				?>
					<h1>
						<?php
							if ($catID != 0)
							{
								echo htmlspecialchars($category['description'], ENT_QUOTES);
							}
							else
							{
								echo "All";
							}
						?> Tutorials
					</h1>
					
					<div>
						<?php
							foreach ($tutorials as $t)
							{
								$image_path = '';
							
								if (file_exists($t['image_path']))
								{
									$image_path = $t['image_path'];
								}
								else
								{
									$image_path = DEFAULT_IMAGE_PATH();
								}
						?>
						
								<div class="tutorial" onclick="window.location='tutorial.php?id=<?php echo $t['id']; ?>'">
									<div class="tutorial-image">
										<img style="float:left;" src="<?php echo $image_path; ?>" width="128" height="128" />
									</div>
									<div class="tutorial-info alpha60">
										<div class="tutorial-title">
											<a class="link-title" href="tutorial.php?id=<?php echo $t['id']; ?>">
												<?php echo $t['title']; ?>
											</a>
										</div>
										<div class="tutorial-author">
											<span style="font-weight:bold;">by</span> <a href="profile.php?id=<?php echo $t['author_id']; ?>"><?php echo $t['author']; ?></a>
										</div>
									</div>
								</div>
						<?php
							}
							
							if (count($tutorials) == 0)
							{
								echo "<div style=\"text-align:center;\">Sorry, no tutorials were found. Please visit a different category.</div>";
							}
						?>
					</div>
				</div>
			</div>
		</div>
		
		<?php
			include('modules/bottom-banner.php');
		?>
	</body>
</html>