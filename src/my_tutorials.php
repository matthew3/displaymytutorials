<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('php-scripts/functions/database.php');
	require_once('php-scripts/classes/BatchQuery.php');
	
	include('php-scripts/functions/constants.php');

	function addError($label, $str)
	{
		if (!isset($_SESSION['tut_manage_errors']))
		{
			$_SESSION['tut_manage_errors'] = array();
		}
		$_SESSION['tut_manage_errors'][] = $str;
	}
	
	//ensure user is logged in
	include('php-scripts/functions/restriction.php');
	echo ensure_user_login(true, 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
	
	/**Initialize variables**/
	$tutorials = array();
	
	/**Retrieve all tutorials created by this user, don't worry about the tutorial's parts for now**/
	$link = openDatabase();
	
	//get tutorial information for all tutorials
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT id, title, image_path FROM tutorials WHERE author_id=? AND deleted=0",
						'i', array($_SESSION['user_id']));
	$tutorials = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve tutorial information. Please try again later.');
	}
	
	unset($q);
	
	mysqli_close($link);
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="http://www.displaymy.com/css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/tutorial.css" />
		<link rel="stylesheet" type="text/css" href="css/my_tutorials.css" />
		
		<link rel="stylesheet" type="text/css" href="modules/css/top-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/lower-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/bottom-banner.css" />
	
		<link rel="stylesheet" type="text/css" href="modules/control-panel/css/control-panel.css" />
		
		<link rel="icon" href="resources/favicon.ico" type="image/x-icon" sizes="16x16" />
		
		<title>DMT - My Tutorials</title>
	</head>
	<body>
		<?php
			include('modules/top-banner.php');
		?>
		
		<div class="middle-banner">
			<div class="main_full">
				<div class="errors">
					<?php
						//If we have errors on this page, output them then clear them
						if (isset($_SESSION['tut_management_errors']))
						{
					?>
							<ul>
						<?php
							foreach ($_SESSION['tut_management_errors'] as $errorStr)
							{
								echo '<li>' . $errorStr . '</li>';
							}
						?>
							</ul>
					<?php
							unset($_SESSION['tut_management_errors']); //clear the errors
						}
					?>
				</div>
			
				<h1>
					My Tutorials
				</h1>
				
				<div style="width:200px; margin:0px auto;">
					<a class="reg-button" style="width:200px;" href="new_tutorial.php?action=new">New Tutorial</a>
				</div>
				
				<!-- Output all tutorials by the current author-->
				<?php
					foreach ($tutorials as $tut)
					{
						$image_path = '';
						
						if (file_exists($tut['image_path']))
						{
							$image_path = $tut['image_path'];
						}
						else
						{
							$image_path = DEFAULT_IMAGE_PATH();
						}
				?>		
						<div class="my-tutorial">
						
							<script type="text/javascript">
								function deleteTutorial(tutName)
								{
									if (confirm('Are you sure you want to delete this tutorial entirely? This cannot be undone!'))
									{
										document.getElementById(tutName).submit();
										return true;
									}
									return false;
								}
							</script>
						
							<div class="tutorial-delete" onclick="deleteTutorial('delete_tut_form<?php echo $tut['id']; ?>')">
								<form id="delete_tut_form<?php echo $tut['id']; ?>" action="php-scripts/delete_tutorial.php" method="post">
									<input type="hidden" name="tut_id" value="<?php echo $tut['id']; ?>" />
										DELETE
										<span id="delete-btn<?php echo $tut['id']; ?>"><input type="submit" value="DELETE" /></span>
										<script type="text/javascript"> document.getElementById('delete-btn<?php echo $tut['id']; ?>').innerHTML = ''; </script>
								</form>
							</div>
							
							<div class="tutorial" onclick="window.location='tutorial.php?id=<?php echo $tut['id']; ?>'">
								
								<div class="tutorial-image">
									<img style="float:left;" src="<?php echo $image_path; ?>" />
								</div>
								
								<div class="tutorial-info alpha60">
									<div id="link<?php echo $tut['id']; ?>" class="tutorial-title">
										<a href="tutorial.php?id=<?php echo $tut['id']; ?>">
											<?php echo $tut['title']; ?>
										</a>
									</div>
								</div>
								<script type="text/javascript"> document.getElementById('link<?php echo $tut['id']; ?>').innerHTML = '<?php echo $tut['title']; ?>'; </script>
							</div>
							
							<div class="tutorial-edit" onclick="document.getElementById('edit_tut_form<?php echo $tut['id']; ?>').submit()">
								<form class="tutorial-edit-form" id="edit_tut_form<?php echo $tut['id']; ?>" action="new_tutorial.php?id=<?php echo $tut['id']; ?>" method="get">
									<input type="hidden" name="action" value="update" />
									<input type="hidden" name="id" value="<?php echo $tut['id']; ?>" />
									EDIT
									<span id="edit-btn<?php echo $tut['id']; ?>"><input type="submit" value="EDIT" /></span>
									<script type="text/javascript"> document.getElementById('edit-btn<?php echo $tut['id']; ?>').innerHTML = ''; </script>
								</form>
							</div>
						</div>
				<?php
					}
				?>
			</div>
		</div>
		
		<?php
			include('modules/bottom-banner.php');
		?>
	</body>
</html>