<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="http://www.displaymy.com/css/main.css" />
		
		<link rel="stylesheet" type="text/css" href="modules/css/top-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/lower-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/bottom-banner.css" />
	
		<link rel="stylesheet" type="text/css" href="modules/control-panel/css/control-panel.css" />
		
		<link rel="icon" href="resources/favicon.ico" type="image/x-icon" sizes="16x16" />
		
		<title>DMT - Tutorial Not Found</title>
	</head>
	<body>
		<?php
			include('modules/top-banner.php');
		?>
		
		<div class="middle-banner">	
			<div class="main_full">
				The tutorial that you were trying to access could not be found.<br />
				Click <a href="category.php" onclick="window.location.history.go(-2); return false;">here</a> to go back.
			</div>
		</div>
		
		<?php
			include('modules/bottom-banner.php');
		?>
	</body>
</html>