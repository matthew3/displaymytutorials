<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('php-scripts/functions/database.php');
	include('php-scripts/classes/BatchQuery.php');
	
	include('php-scripts/functions/restriction.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['tutorial_errors']))
		{
			$_SESSION['tutorial_errors'] = array();
		}
		$_SESSION['tutorial_errors'][] = $str;
	}

	//must provide some tutorial id
	if (!isset($_GET['id']))
	{
		$_SESSION['tut_not_found_errors'] = 'You were redirected from tutorial.php to here because no tutorial id was specified.';
		header('Location: tutorial_not_found.php');
		exit();
	}
	
	$tutID = intval($_GET['id']);
	
	$sectID = 0;
	if (isset($_GET['sect']))
	{
		$sectID = intval($_GET['sect']);
	}
	
	$link = openDatabase();
	
	//ensure user is owner of this tutorial, get the title while we're at it
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT t.title, t.author_id, t.image_path, u.user FROM tutorials AS t, displaymy_db.users AS u WHERE t.id=? AND t.deleted=0 AND u.id = t.author_id",
						'i', array($tutID));
	$info = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve tutorial information. Please try again later.');
	}
	
	if (empty($info) or $info == null)
	{
		mysqli_close($link);
		header('Location: tutorial_not_found.php');
		exit();
	}
	
	unset($q);
	
	$IS_AUTHOR = false;
	$info = $info[0];
	$realAuthor = $info['author_id'];
	$tutTitle = $info['title'];
	
	if (isset($_SESSION['user_id']) and $realAuthor == $_SESSION['user_id'])
	{
		$IS_AUTHOR = true;
	}
	
	/**Initialize variables**/
	$sections = array();
	
	/**Retrieve all tutorial sections for this tutorial**/
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT id, title FROM sections WHERE tut_id=? AND parent_id=0 AND deleted=0 ORDER BY sequence_num ASC",
								'i', array($tutID));
	$sections = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve tutorial information. Please try again later.');
	}
	
	unset($q);
	
	/*Retrieve information concerning chosen section*/
	$q = new BatchQuery($link);
	if ($sectID != 0) //get the section asked for
	{
		$q->addParamQuery("SELECT id, sequence_num, title, html, parent_id, creation_date, last_edited, view_count, up_count, down_count FROM sections WHERE tut_id=? AND id=? AND deleted=0",
							'ii', array($tutID, $sectID));
	}
	else //no sectID specified, get the first (minimum seq_id) section from all sections of this tutorial
	{
		$q->addParamQuery("SELECT id, sequence_num, title, html, parent_id, creation_date, last_edited, view_count, up_count, down_count FROM sections WHERE tut_id=? AND parent_id=0 AND deleted=0 AND
							sequence_num=(SELECT MIN(sequence_num) FROM sections WHERE tut_id=? AND parent_id=0 GROUP BY tut_id)",
							'ii', array($tutID, $tutID));
	}
	
	$sect = $q->execute();
	$sectSet = false;
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve tutorial information. Please try again later.');
	}
	else
	{
		if (count($sect) > 0)
		{
			$sect = $sect[0];
			$sectSet = true;
		}
		else
		{
			addError('invalid_section', 'This is an invalid section. Please choose from the legend to the left.');
		}
	}
	
	unset($q);
	
	//use the GET parameter to select the current section
	$children = array();
	
	if ($sectSet)
	{
		$parentID = $sect['id'];
		if ($sect['parent_id'] != 0)
		{
			$parentID = $sect['parent_id'];
		}
	
		//get all children of the current section
		$q = new BatchQuery($link);
		$q->addParamQuery("SELECT id, title FROM sections WHERE parent_id=? AND deleted=0",
							'i', array($parentID));
		$children = $q->execute();
		
		if ($q->anyErrors())
		{
			addError('database','Could not retrieve tutorial information. Please try again later.');
		}
		
		unset($q);
	}
	
	//$link->close(); //this is done in the php code near the bottom for comments
	
	/*META data*/
	/*Get the first paragraph to use as a description*/
	$description = '';
	
	if ($sectSet)
	{
		//find first paragraph start tag
		$pattern = "#<p[^>]*>#i";
		preg_match($pattern, $sect['html'], $matches);
		$endOfFirstPTag = 0;
		if (count($matches) > 0)
		{
			$endOfFirstPTag = strpos($sect['html'], $matches[0]) + strlen($matches[0]);
		}
		else
		{
			$descLen = -1; //make search invalid
		}
		
		//find first paragraph end tag
		$pattern = "#</p>#i";
		preg_match($pattern, $sect['html'], $matches);
		$endOfFirstEndTag = 0;
		if (count($matches) > 0)
		{
			$endOfFirstEndTag = strpos($sect['html'], $matches[0]) + strlen($matches[0]);
		}
		else
		{
			$descLen = -1; //make search invalid
		}
		
		if ($descLen != -1)
		{
			$descLen = ($endOfFirstEndTag - $endOfFirstPTag);
		}
		
		//we were unable to find a valid paragraph using the tags
		if (($endOfFirstEndTag == false) || ($endOfFirstPTag == false) || ($descLen <= 0))
		{
			$description = $tutTitle + ": " + $sect['title'];
		}
		else //just use the titles as the description
		{
			$description = substr($sect['html'], $endOfFirstPTag, $descLen);
		}
	}
	
	$descriptionMeta = $description;
	$descriptionOG = $description;
	$descriptionTwit = $description;
	
	if (strlen($descriptionMeta) > 160)
	{
		$descriptionMeta = substr($description, 0, 159);
	}
	if (strlen($descriptionTwit) > 200)
	{
		$descriptionTwit = substr($description, 0, 199);
	}
	if (strlen($descriptionOG) > 297)
	{
		$descriptionOG = substr($description, 0, 296);
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" type="text/css" href="http://www.displaymy.com/css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/tutorial.css" />
		
		<link rel="stylesheet" type="text/css" href="modules/css/top-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/lower-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/bottom-banner.css" />
	
		<link rel="stylesheet" type="text/css" href="modules/control-panel/css/control-panel.css" />
		
		<script type="text/javascript" src="javascript/libraries/jquery-1.11.1.min.js"></script>
		
		<link rel="icon" href="resources/favicon.ico" type="image/x-icon" sizes="16x16" />
		
		<title>DMT - Tutorial - <?php echo $tutTitle; ?><?php echo (isset($sect['title'])? (' - ' . $sect['title']) : ''); ?></title>
		
		<meta charset="utf-8"> 
		
		<!-- general meta (google) -->
		<meta name="description" content="<?php echo $descriptionMeta; ?>" />
		
		<!-- open graph meta (facebook) -->
		<meta property="og:title" content="<?php echo $tutTitle; ?>" />
		<meta property="og:type" content="article" />
		<meta property="og:image" content="<?php echo $info['image_path']; ?>"/>
		<meta property="og:url" content="http://www.displaymy.com/tutorial?id=<?php echo $tutID; ?>&sect=<?php echo $sect['id']; ?>" />
		<meta property="og:description" content="<?php echo $descriptionOG; ?>" />
		
		<!-- twitter meta -->
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:url" content="http://www.displaymy.com/tutorial?id=<?php echo $tutID; ?>&sect=<?php echo $sect['id']; ?>" />
		<meta name="twitter:title" content="<?php echo $tutTitle; ?>" />
		<meta name="twitter:description" content="<?php echo $descriptionTwit; ?>" />
		<meta name="twitter:image" content="<?php echo $info['image_path']; ?>" />
	</head>
	<body>
		<?php
			include('modules/top-banner.php');
		?>
		
		<div class="middle-banner">
			<div class="left-column">
				<?php
					if ($sectSet)
					{
				?>
						<div class="tut_info">
						
							<div class="profile">
								<img class="profile_pic" />
								<div class="profile_info">
									<span class="small_text">Written By:</span>
									<br />
									<a href="profile.php?id=<?php echo $info['author_id']; ?>"><?php echo $info['user']; ?></a>
								</div>
							</div>
							
							<div class="stats">
								
								<div class="meter_area">
									<div class="meter">
										<div class="meter_bar_r"></div>
										<div class="meter_bar_g" style="width:<?php
											$up = intval($sect['up_count']);
											$down = intval($sect['down_count']);
											$sum = $up + $down;
											if ($sum != 0)
											{
												$percent = intval(((float)$up / (float)$sum) * 100);
											}
											else
											{
												$percent = 100;
											}
											
											echo $percent;
											?>%;"></div>
									</div>
								</div>
								
								<script type="text/javascript">
									window.localStorage.clear(); //TODO REMOVE!!
								
									function disableVoting(voteType)
									{
										var btn = document.getElementById('up_btn');
										btn.onclick = function(){};
										if (voteType == 1)
										{
											btn.className = "up_btn up_btn_chosen";
										}
										else
										{
											btn.className = "up_btn btn_disabled";
										}
										
										btn = document.getElementById('down_btn');
										btn.onclick = function(){};
										if (voteType == 0)
										{
											btn.className = "down_btn down_btn_chosen";
										}
										else
										{
											btn.className = "down_btn btn_disabled";
										}
									}
								
									function count_vote(voteType)
									{	
										if (localStorage.getItem(voteName) != null)
										{
											return;
										}
										
										$.post("php-scripts/vote.php", {section_id: <?php echo $sect['id']; ?>,
																					vote_type: voteType}, function(result)
										{
											
											if (result == 'true')
											{
												localStorage.setItem(voteName, voteType);
												localStorage.getItem(voteName); //chrome bug
												
												disableVoting(voteType);
												
												if (voteType == 1)
												{
													var upCount = document.getElementById('up_count');
													upCount.innerHTML = (parseInt(upCount.innerHTML, 10) + 1);
												}
												else
												{
													var downCount = document.getElementById('down_count');
													downCount.innerHTML = (parseInt(downCount.innerHTML, 10) + 1);
												}
												
												document.getElementById('vote_message').innerHTML = '';
											}
											else
											{
												document.getElementById('vote_message').innerHTML = result;
											}
										});
									}
								</script>
								
								<div id="votes" class="votes">
									<div id="vote_message" style="text-align:center;"></div>
									
									<div id="up_btn" class="up_btn" onclick="count_vote(1)"> <img src="resources/images/up_vote.png" /> </div>
									<div id="up_count" class="up_count"><?php echo $sect['up_count']; ?></div>
									
									<div id="down_btn" class="down_btn" onclick="count_vote(0)"> <img src="resources/images/down_vote.png" /> </div>
									<div id="down_count" class="down_count"><?php echo $sect['down_count']; ?></div>
								</div>
								
								<div class="view_count">
									<?php echo $sect['view_count']; ?> views
								</div>
								
								<div class="flag_area">
									<div class="flag" id="flag" onclick="flag_section()">
										<img src="resources/images/flag.png" title="Flag as inappropriate" />
									</div>
								</div>
								
								<script type="text/javascript">
									
									//take care of cookie storage for votes (not allowing users to vote again)
									//output the appropriate html
									var voteName = 'vote-<?php echo $sect['id']; ?>';
									if (localStorage.getItem(voteName) != null)
									{
										var upOrDown = localStorage.getItem(voteName);
									
										disableVoting(upOrDown);
									}
									
									var flagName = 'flag-<?php echo $sect['id']; ?>';
									if (localStorage.getItem(flagName) != null)
									{
										var flagged = localStorage.getItem(flagName);
										
										if (flagged == 'flagged')
										{
											document.getElementById('flag').innerHTML = 'Flagged as inappropriate';
										}
										else if (flagged == 'true')
										{
											document.getElementById('flag').innerHTML = '<img src=\"resources/images/flag.png\" title=\"Flag as inappropriate\" />';
										}
									}
									
									function flag_section()
									{
										if (localStorage.getItem(flagName) != null)
										{
											return;
										}
										
										$.post("php-scripts/flag.php", {section_id: <?php echo $sect['id']; ?>}, function(result)
										{
											if (result == 'false')
											{
											}
											else if (result == 'true')
											{
												localStorage.setItem(flagName, 'flagged');
												localStorage.getItem(flagName); //chrome bug
												document.getElementById('flag').innerHTML = 'Flagged as inappropriate';
											}
										});
									}
								</script>
							
							</div>
						</div>
				<?php
					}
				?>
						<div class="legend">
							<span style="font-size:20px; font-weight:bold;"><?php echo $tutTitle; ?></span>
							<h2>
								Sections
							</h2>
							
							<?php
								foreach ($sections as $legend_sect)
								{
							?>
									<a class="legend-item <?php echo ($legend_sect['id'] == $sect['id'])? 'selected-item' : ''; ?>" href="http://tutorials.displaymy.com/tutorial.php?id=<?php echo $tutID; ?>&amp;sect=<?php echo $legend_sect['id']; ?>">
										<?php echo $legend_sect['title']; ?>
									</a>
								<?php
									if ($sectSet)
									{
										if ($legend_sect['id'] == $sect['id'] or $legend_sect['id'] == $sect['parent_id'])
										{
											foreach ($children as $child)
											{
								?>
												<a class="legend-item-child <?php echo ($child['id'] == $sect['id'])? 'selected-item-child' : ''; ?>" href="http://tutorials.displaymy.com/tutorial.php?id=<?php echo $tutID; ?>&amp;sect=<?php echo $child['id']; ?>">
													<?php echo $child['title']; ?>
												</a>
								<?php
											}
										}
									}
								}
							?>
							
							<?php
								//current user is the author of this tutorial
								if ($IS_AUTHOR)
								{
							?>
									<div style="margin-top:5px;">
										<a class="reg-button" onclick="window.location='new_section.php?id=<?php echo $tutID; ?>&action=new'">New Section</a>
									</div>
							<?php
								}
							?>
						</div>
			</div>
			
			<div class="right-column">
				<div class="main">
					<?php
						
						//If we have errors on this page, output them then clear them
						if (isset($_SESSION['tutorial_errors']))
						{
					?>
							<div class="errors">
								<ul>
							<?php
								foreach ($_SESSION['tutorial_errors'] as $errorStr)
								{
									echo '<li>' . $errorStr . '</li>';
								}
							?>
								</ul>
							</div>
					<?php
							unset($_SESSION['tutorial_errors']); //clear the errors
						}
						else
						{
							if ($sectSet)
							{
							
								if ($IS_AUTHOR)
								{
						?>
									<div class="author-options">
										<a href="new_section.php?id=<?php echo $tutID; ?>&amp;sect_id=<?php echo $sect['id']; ?>&amp;action=update" class="reg-button">Edit</a>
										<div style="float:right;">
											<form action="php-scripts/delete_section.php" method="post" onsubmit="return confirm('Are you sure you want to delete this section? All children sections of this section will be deleted as well. This cannot be undone.')">
												<input type="hidden" name="tut_id" value="<?php echo $tutID; ?>" />
												<input type="hidden" name="sect_id" value="<?php echo $sect['id']; ?>" />
												<input type="submit" class="reg-button" value="DELETE" />
											</form>
										</div>
									</div>
						<?php
								}
						?>		
								<h1 style="font-size:34px; text-align:center; border-bottom:none; color:black;">
									<?php echo $sect['title']; ?>
								</h1>
								
						<?php
								echo html_entity_decode($sect['html']);
								
								//count a view (insert a record into temp table)
							}
							else if ($IS_AUTHOR)
							{
						?>
								This tutorial has no sections yet,
								<a href="new_section.php?id=<?php echo $tutID; ?>"> make one</a>
						<?php
							}
							else
							{
						?>
								Sorry, this tutorial has no sections yet.
						<?php
							}
						}
					?>
				</div>
				
				<div class="comments-area">
					<div id="comments" class="comments">
					</div>
					
					<div id="pager" class="pager">
					</div>
				
					<?php
						if (ensure_user_login(false) and $sectSet)
						{
					?>
							<div id="comment-box" class="comment-box">
								<h4>Comment</h4>
								<form action="post_comment.php" method="post" onsubmit="postComment(); return false;">
									<input type="hidden" name="section_id" value="<?php echo $sect['id']; ?>" />
									<textarea id="comment" class="comment-box-textarea" name="comment" maxlength="300"></textarea>
									<input class="reg-button" style="width:150px; margin-left:280px;" type="submit" value="Post Comment" />
								</form>
							</div>
					<?php
						}
					?>
				</div>
			</div>
			
			<?php
				if ($sectSet)
				{
					$commentCount = 0;
					
					/*get all tutorials that have a certain parent ID*/
					$q = new BatchQuery($link);
					$q->addParamQuery("SELECT COUNT(id) AS comment_count FROM comments WHERE sect_id=?",
										'i', array($sect['id']));
					$comment = $q->execute();
					
					if (!$q->anyErrors())
					{
						$commentCount = $comment[0]['comment_count'];
					}
					
					unset($q);
					
					mysqli_close($link);
			?>
					<script type="text/javascript">
						/*All of this javascript will query for comments, if it finds comments it will make page
						numbers under the pager*/
						var pageNum = 1;
						
						function getPosts(newPageNum)
						{
							pageNum = newPageNum;
							$.post("php-scripts/get_comments.php", {sect_id: <?php echo $sect['id']; ?>,
																	page_num: pageNum},
																	function(result)
									{
										document.getElementById('comments').innerHTML = result;
									});
									
							var pagerContent = '';
							var num = 0;
							//list page numbers up to a maximum of 7 total
							for (var i = pageNum-3; (i <= <?php echo intval($commentCount/10)+1; ?>) && (num < 7); i++)
							{
								if (i > 0)
								{
									pagerContent += '<div id="pageNum' + i + '" class="pager-num" onclick="getPosts(' + i + ')">' + i + '</div>';
									num++;
								}
							}
							
							document.getElementById('pager').innerHTML = pagerContent;
							document.getElementById('pageNum' + pageNum).style.backgroundColor = '#9999AA'; //set selected pageNum
						}
						
						getPosts(pageNum);
						
						function postComment()
						{
							$.post("php-scripts/post_comment.php", {sect_id: <?php echo $sect['id']; ?>,
																	comment: document.getElementById('comment').value},
																	function(result)
									{
										document.getElementById('comments').innerHTML += result;
										document.getElementById('comment').value = '';
									});
						}
					</script>
			<?php
				}
			?>
			
		</div>
		
		<?php
			include('modules/bottom-banner.php');
		?>
		
		<?php
			if ($sectSet)
			{
		?>
				<script type="text/javascript">				
					if (localStorage.getItem('viewed') == null)
					{
						$.post("php-scripts/view.php", {section_id: <?php echo $sect['id']; ?>}, function(result)
						{
							if (result == 'false')
							{
							}
							else if (result == 'true')
							{
								localStorage.setItem('viewed', 'true');
								localStorage.getItem('viewed'); //chrome bug
							}
						});
					}
				</script>
		<?php
			}
		?>
	</body>
</html>