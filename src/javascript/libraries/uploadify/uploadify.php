<?php
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include($_SERVER['DOCUMENT_ROOT'] . "/php-scripts/functions/image.php");
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/

// Define a destination
$targetFolder = '/sp/' . $_SESSION['user']; // Relative to the root

if (!empty($_FILES)) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
	
	if (!is_dir($targetPath))
	{
		mkdir($targetPath);
	}
	
	//validate file size
	list($width, $height, $type, $attr) = getimagesize($_FILES["Filedata"]['tmp_name']);
	
	// Validate the file type
	$fileTypes = array('jpg','jpeg','gif','png'); // File extensions
	$fileParts = pathinfo($_FILES['Filedata']['name']);
	
	if (in_array($fileParts['extension'],$fileTypes)) {
		
		//ensure unique file name
		$saveName = $_FILES['Filedata']['name'];
		while (file_exists($targetPath . '/' . $saveName) and (strlen($saveName) < 256))
		{
			//replace image filename with same name including a random number before ext
			$saveName = preg_replace('#([^\\.]+)(\\.[a-zA-Z]+)#i', '${1}' . rand(0, 9) . '$2', $saveName);
		}
	
		$uploadPath = $targetPath . '/' . $saveName;
		
		//resize the image if necessary
			$resize = false;
			$outputWidth = $width;
			$ratio = $width/$height;
			if ($width > 700)
			{
				$ratio = 700/$width;
				$outputWidth = 700;
				$resize = true;
				$height = $height * $ratio; //keep the image scaled if we changed the width
			}
			
			$outputHeight = $height;
			if ($height > 700)
			{
				$ratio = 700/$height;
				$outputHeight = 700;
				$resize = true;
				$outputWidth = $outputWidth * $ratio; //keep width scaled to changed height
			}
			
			if ($resize)
			{
				generate_image_thumbnail($tempFile, $uploadPath, intval($outputWidth), intval($outputHeight));
			}
			else
			{
				move_uploaded_file($tempFile, $uploadPath);
			}
		
		//get the new size after making thumbnail
		$size = filesize($uploadPath) / 1024; //size of file in kb
		
		if ($_SESSION['uploads_size'] + $size > 700) //this image would throw user over their section limit
		{
			echo "[This image of " . $size . "kb would exceed memory limits. (currently at " . $_SESSION['uploads_size'] . "/700kb)]";
			unlink($uploadPath); //delete the image we created
			exit;
		}
		
		$_SESSION['uploads_size'] += $size; //successful upload, add to size
		
		//return the image response
		echo '<a href="http://' . $_SERVER['SERVER_NAME'] . '/sp/' . $_SESSION['user'] . '/' . $saveName . '"><img src="http://' . $_SERVER['SERVER_NAME'] . '/sp/' . $_SESSION['user'] . '/' . $saveName . '" width="' . $outputWidth . '" height="' . $outputHeight . '" /></a>';
		
	} else {
		echo 'Invalid file type.';
	}
}
?>