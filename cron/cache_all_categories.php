<?php
	include('/srv/http/tutorials.displaymy.com/public_html/php-scripts/functions/database.php');
	include('/srv/http/tutorials.displaymy.com/public_html/php-scripts/classes/BatchQuery.php');
	
	/*
		This is a recursive function to print out the tree structure hierarchy that is a taxonomy
	*/
	function outputChild($link, &$parent, $tabNum)
	{	
		
		$q = new BatchQuery($link);
		$q->addParamQuery("SELECT a.id AS id, a.description AS description, a.parent_id AS parent_id, (SELECT count(*) FROM categories b WHERE b.parent_id=a.id) AS num_children
							FROM categories AS a WHERE a.parent_id=? ORDER BY description",
							'i', array($parent['id']));
		$cats = $q->execute();
		
		if ($q->anyErrors())
		{
			unset($q);
			mysqli_close($link);
			echo '';
			exit();
		}
		
		if (count($cats) == 0) //base case
		{
			return '';
		}
		
		//set offset for children and children of children
		$tabs = '';
		for ($i=0; $i < $tabNum; $i++)
		{
			$tabs .= '&nbsp;&nbsp;&nbsp;';
		}
		
		$output2 = '';
		foreach ($cats as $cat)
		{
			$output2 .= '<option value="' . $cat['id'] . '">' . $tabs . $cat['description'] . '</option>';
			$output2 .= outputChild($link, $cat, $tabNum+1); //recursive step, print all children of child
		}
		
		$output2 .= '<option value="' . $parent['id'] . '">' . $tabs . 'Other</option>'; //plus extra space to align with children
		
		return $output2;
	}
	
	$link = openDatabase();
	
	$q = new BatchQuery($link);
	$q->addQuery("SELECT id, description, parent_id FROM categories WHERE parent_id=0 AND id!=0 ORDER BY description"); //no languages and no "None" category
	$parents = $q->execute();
	
	if ($q->anyErrors())
	{
		unset($q);
		mysqli_close($link);
		echo '';
		exit();
	}
	
	$output = '<select id="categories_1" onchange="window.location.replace(\\\'http://tutorials.displaymy.com/category.php?cat_id=\\\' + document.getElementById(\\\'categories_1\\\').value)">';
	
	$output .= '<option value="0">---</option>';
	$output .= '<option value="0">All Categories</option>';
	
	foreach ($parents as $parent)
	{
		$output .= '<option value="' . $parent['id'] . '">' . $parent['description'] . '</option>';
		$output .= outputChild($link, $parent, 1);
	}
	
	mysqli_close($link);
			
	$output .= '</select>';
	
	file_put_contents('/srv/http/tutorials.displaymy.com/public_html/resources/all_categories.php', "<?php echo '" . $output . "'; exit; ?>");
	
	exit;
?>
