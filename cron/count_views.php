<?php
	session_start();
	
	include('/srv/http/tutorials.displaymy.com/public_html/php-scripts/functions/database.php');
	require_once('/srv/http/tutorials.displaymy.com/public_html/php-scripts/classes/BatchQuery.php');

	$link = openDatabase();
	
	/*Find out the maximum amount of bytes that can be in a packet from mysql server*/
	$q = new BatchQuery($link);
	$q->addQuery("SELECT @@global.max_allowed_packet AS max_packets");
	$maxPackets = $q->execute();

	$maxPackets = $maxPackets[0]['max_packets'];
	
	unset($q);
	
	/*Count all section views*/
	$q = new BatchQuery($link);
	$q->addQuery("SELECT section_id, COUNT(*) AS view_count FROM views_temp GROUP BY section_id");
	$results = $q->execute();

	unset($q);
	
	$link->autocommit(false);
	
	$amountOfBytes = 0;
	$rowCount = 0;
	$numOfRows = count($results);
	
	$q = new BatchQuery($link);
	
	while ($rowCount < $numOfRows)
	{
		$row = $results[$rowCount];
		
		$str = "UPDATE sections SET view_count=view_count+? WHERE id=?";
		$amountOfBytes += strlen($str) + strlen($row['view_count']) + strlen($row['section_id']); //calc length of query string + length of parameters
	
		$str2 = "DELETE FROM views_temp WHERE section_id=?";
		$amountOfBytes += strlen($str2) + strlen($row['section_id']);
	
		if ($amountOfBytes + strlen($str) + strlen($str2) + 12 < $maxPackets) //12 is 2 * (strlen("commit")) to make sure we have room for the commit statement
		{
			$q->addParamQuery($str, 'ii', array($row['view_count'], $row['section_id']));
			$q->addParamQuery($str2, 'i', array($row['section_id']));
			
			$rowCount++; //go to next id
		}
		else //we've reached the maximum amount of packets, execute and reset
		{
			$q->addQuery("COMMIT"); //add the commit
			$q->execute();
			unset($q);
			$q = new BatchQuery();
			$amountOfBytes = 0;
		}
	}
	
	//some sort of query still exists, execute it
	if ($amountOfBytes != 0)
	{
		$q->addQuery("COMMIT");
		$q->execute();
	}

	unset($q);
	
	/*Update tutorial views, based on all child section views, in batch*/
	/*If this fails, tutorial view counts won't be updated until another view of one of its sections changes (not a big deal)*/
	/*
	$link->autocommit(true);
	
	$amountOfBytes = 0;
	$rowCount = 0;
	$numOfTutorials = count($tutorialIdsAffected);
	
	$q = new BatchQuery($link);
	
	while ($rowCount < $numOfTutorials)
	{
		$tut_id = $tutorialIdsAffected[$rowCount]['tut_id'];
		
		$str = "UPDATE tutorials SET view_count=(SELECT SUM(view_count) FROM sections WHERE tut_id=? GROUP BY tut_id) WHERE id=?";
		$amountOfBytes = $amountOfBytes + strlen($str) + strlen($tut_id)*2; //calc length of query string + length of parameters
	
		//adds commit to the byte calculation, but doesn't actually commit. This is to avoid the overhead of committing many times
		//the 12 means strlen("COMMIT")*2
		if (($amountOfBytes + strlen($str)  + 12 + strlen($tut_id)*2) < $maxPackets)
		{
			$q->addParamQuery($str, 'ii', array($tut_id, $tut_id));
			$rowCount++; //go to next id
		}
		else //we've reached the maximum amount of packets, execute and reset
		{
			$q->addQuery("COMMIT");
			$q->execute();
			unset($q);
			$q = new BatchQuery();
			$amountOfBytes = 0;
		}
	}
	
	//some sort of query still exists, execute it
	if ($amountOfBytes != 0)
	{
		$q->execute();
	}
	
	unset($q);
	*/
	
	$link->close();
	
	exit();
?>
