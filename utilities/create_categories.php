<?php
	include('/srv/http/tutorials.displaymy.com/public_html/php-scripts/functions/database.php');
	include('/srv/http/tutorials.displaymy.com/public_html/php-scripts/classes/BatchQuery.php');
	
	$link = openDatabase();
	
	$q = new BatchQuery($link);
//	$q->addQuery("INSERT INTO categories VALUES (0, 'None', NULL)");
		$q->addQuery("INSERT INTO categories VALUES (1, 'Arts & Culture', 0)");
			$q->addQuery("INSERT INTO categories VALUES (2, 'Crafts', 1)");
			$q->addQuery("INSERT INTO categories VALUES (3, 'Literature', 1)");
			$q->addQuery("INSERT INTO categories VALUES (4, 'Movies', 1)");
			$q->addQuery("INSERT INTO categories VALUES (5, 'Music', 1)");
			$q->addQuery("INSERT INTO categories VALUES (6, 'Religion', 1)");
			$q->addQuery("INSERT INTO categories VALUES (7, 'Threatre', 1)");
			$q->addQuery("INSERT INTO categories VALUES (8, 'Visual Arts', 1)");
				$q->addQuery("INSERT INTO categories VALUES (9, 'Animation', 8)");
				$q->addQuery("INSERT INTO categories VALUES (10, 'Drawing', 8)");
				$q->addQuery("INSERT INTO categories VALUES (11, 'Sculpting', 8)");
				$q->addQuery("INSERT INTO categories VALUES (12, 'Painting', 8)");
				$q->addQuery("INSERT INTO categories VALUES (13, 'Photography', 8)");
	
		$q->addQuery("INSERT INTO categories VALUES (14, 'Beauty & Style', 0)");
			$q->addQuery("INSERT INTO categories VALUES (15, 'Accessories', 14)");
			$q->addQuery("INSERT INTO categories VALUES (16, 'Clothes', 14)");
			$q->addQuery("INSERT INTO categories VALUES (17, 'Fashion', 14)");
			$q->addQuery("INSERT INTO categories VALUES (18, 'Hair', 14)");
			$q->addQuery("INSERT INTO categories VALUES (19, 'Make-Up', 14)");
			
		$q->addQuery("INSERT INTO categories VALUES (20, 'Business & Economy', 0)");
			$q->addQuery("INSERT INTO categories VALUES (21, 'Accounting', 20)");
			$q->addQuery("INSERT INTO categories VALUES (22, 'Financing', 20)");
			$q->addQuery("INSERT INTO categories VALUES (23, 'Marketing', 20)");
			$q->addQuery("INSERT INTO categories VALUES (24, 'Small Business', 20)");
			
		$q->addQuery("INSERT INTO categories VALUES (25, 'Home Renovation', 0)");
			$q->addQuery("INSERT INTO categories VALUES (26, 'Exterior Decorating', 25)");
			$q->addQuery("INSERT INTO categories VALUES (27, 'Interior Decorating', 25)");
			$q->addQuery("INSERT INTO categories VALUES (28, 'Landscaping', 25)");
			
		$q->addQuery("INSERT INTO categories VALUES (112, 'Lifestyle & Living', 0)");
			$q->addQuery("INSERT INTO categories VALUES (113, 'Diet', 112)");
			$q->addQuery("INSERT INTO categories VALUES (114, 'Spiritual Wellness', 112)");
			$q->addQuery("INSERT INTO categories VALUES (115, 'Self-Help', 112)");
			$q->addQuery("INSERT INTO categories VALUES (116, 'Personal Finances', 112)");
			
		$q->addQuery("INSERT INTO categories VALUES (29, 'Transportation', 0)");
			$q->addQuery("INSERT INTO categories VALUES (30, 'Airplanes & Airliners', 29)");
			$q->addQuery("INSERT INTO categories VALUES (31, 'Bikes', 29)");
			$q->addQuery("INSERT INTO categories VALUES (32, 'Boats', 29)");
			$q->addQuery("INSERT INTO categories VALUES (33, 'Cars', 29)");
			$q->addQuery("INSERT INTO categories VALUES (34, 'Public Transport', 29)");
			
		$q->addQuery("INSERT INTO categories VALUES (35, 'Electronics', 0)");
			$q->addQuery("INSERT INTO categories VALUES (36, 'Entertainment System', 35)");
			$q->addQuery("INSERT INTO categories VALUES (37, 'Hand-Held', 35)");
			
		$q->addQuery("INSERT INTO categories VALUES (38, 'Math & Science', 0)");
			$q->addQuery("INSERT INTO categories VALUES (39, 'Biology', 38)");
			$q->addQuery("INSERT INTO categories VALUES (40, 'Chemistry', 38)");
			$q->addQuery("INSERT INTO categories VALUES (41, 'Earth Science', 38)");
			$q->addQuery("INSERT INTO categories VALUES (42, 'Mathematics', 38)");
			$q->addQuery("INSERT INTO categories VALUES (43, 'Physics', 38)");
			$q->addQuery("INSERT INTO categories VALUES (44, 'Statistics', 38)");
			
		$q->addQuery("INSERT INTO categories VALUES (45, 'Applied Science', 0)");
			$q->addQuery("INSERT INTO categories VALUES (46, 'Agriculture', 45)");
			$q->addQuery("INSERT INTO categories VALUES (47, 'Architecture & Design', 45)");
			$q->addQuery("INSERT INTO categories VALUES (48, 'Engineering', 45)");
			$q->addQuery("INSERT INTO categories VALUES (49, 'Health Care Science', 45)");
			
		$q->addQuery("INSERT INTO categories VALUES (50, 'Languages & Linguistics', 0)");
			$q->addQuery("INSERT INTO categories VALUES (51, 'Languages', 50)");
				$q->addQuery("INSERT INTO categories VALUES (52, 'Arabic', 51)");
				$q->addQuery("INSERT INTO categories VALUES (53, 'Bengali', 51)");
				$q->addQuery("INSERT INTO categories VALUES (54, 'Cantonese', 51)");
				$q->addQuery("INSERT INTO categories VALUES (55, 'Dutch', 51)");
				$q->addQuery("INSERT INTO categories VALUES (56, 'English', 51)");
				$q->addQuery("INSERT INTO categories VALUES (57, 'French', 51)");
				$q->addQuery("INSERT INTO categories VALUES (58, 'German', 51)");
				$q->addQuery("INSERT INTO categories VALUES (59, 'Hindi', 51)");
				$q->addQuery("INSERT INTO categories VALUES (60, 'Italian', 51)");
				$q->addQuery("INSERT INTO categories VALUES (61, 'Japanese', 51)");
				$q->addQuery("INSERT INTO categories VALUES (62, 'Javanese', 51)");
				$q->addQuery("INSERT INTO categories VALUES (63, 'Korean', 51)");
				$q->addQuery("INSERT INTO categories VALUES (64, 'Malay/Indonesian', 51)");
				$q->addQuery("INSERT INTO categories VALUES (65, 'Mandrin', 51)");
				$q->addQuery("INSERT INTO categories VALUES (66, 'Marathi', 51)");
				$q->addQuery("INSERT INTO categories VALUES (67, 'Persian', 51)");
				$q->addQuery("INSERT INTO categories VALUES (68, 'Polish', 51)");
				$q->addQuery("INSERT INTO categories VALUES (69, 'Portuguese', 51)");
				$q->addQuery("INSERT INTO categories VALUES (70, 'Punjabi', 51)");
				$q->addQuery("INSERT INTO categories VALUES (71, 'Russian', 51)");
				$q->addQuery("INSERT INTO categories VALUES (72, 'Spanish', 51)");
				$q->addQuery("INSERT INTO categories VALUES (73, 'Tamil', 51)");
				$q->addQuery("INSERT INTO categories VALUES (74, 'Telugu', 51)");
				$q->addQuery("INSERT INTO categories VALUES (75, 'Thai', 51)");
				$q->addQuery("INSERT INTO categories VALUES (76, 'Turkish', 51)");
				$q->addQuery("INSERT INTO categories VALUES (77, 'Urdu', 51)");
				$q->addQuery("INSERT INTO categories VALUES (78, 'Vietnamese', 51)");
				$q->addQuery("INSERT INTO categories VALUES (79, 'Wu', 51)");
				
		$q->addQuery("INSERT INTO categories VALUES (80, 'Computers & Networks', 0)");
				$q->addQuery("INSERT INTO categories VALUES (81, 'Coding', 80)");
				$q->addQuery("INSERT INTO categories VALUES (82, 'Hardware', 80)");
				$q->addQuery("INSERT INTO categories VALUES (83, 'Networks', 80)");
				$q->addQuery("INSERT INTO categories VALUES (84, 'Software', 80)");
				$q->addQuery("INSERT INTO categories VALUES (85, 'Theoretical Comp Sci', 80)");
				$q->addQuery("INSERT INTO categories VALUES (86, 'Website Design', 80)");
				$q->addQuery("INSERT INTO categories VALUES (87, 'Website Development', 80)");
				
		$q->addQuery("INSERT INTO categories VALUES (88, 'Sports', 0)");
			$q->addQuery("INSERT INTO categories VALUES (89, 'Baseball', 88)");
			$q->addQuery("INSERT INTO categories VALUES (90, 'Basketball', 88)");
			$q->addQuery("INSERT INTO categories VALUES (91, 'Biking', 88)");
			$q->addQuery("INSERT INTO categories VALUES (92, 'Cricket', 88)");
			$q->addQuery("INSERT INTO categories VALUES (93, 'Golf', 88)");
			$q->addQuery("INSERT INTO categories VALUES (94, 'Hockey', 88)");
			$q->addQuery("INSERT INTO categories VALUES (95, 'Football', 88)");
			$q->addQuery("INSERT INTO categories VALUES (96, 'Rugby', 88)");
			$q->addQuery("INSERT INTO categories VALUES (97, 'Running', 88)");
			$q->addQuery("INSERT INTO categories VALUES (98, 'Skateboarding', 88)");
			$q->addQuery("INSERT INTO categories VALUES (99, 'Soccer', 88)");
			$q->addQuery("INSERT INTO categories VALUES (100, 'Tennis', 88)");
			$q->addQuery("INSERT INTO categories VALUES (101, 'Volleyball', 88)");
			
		$q->addQuery("INSERT INTO categories VALUES (102, 'Games', 0)");
			$q->addQuery("INSERT INTO categories VALUES (103, 'Board Game', 102)");
			$q->addQuery("INSERT INTO categories VALUES (104, 'Card Game', 102)");
			$q->addQuery("INSERT INTO categories VALUES (105, 'Video Game', 102)");
			
		$q->addQuery("INSERT INTO categories VALUES (106, 'Social Sciences & Politics', 0)");
			$q->addQuery("INSERT INTO categories VALUES (107, 'Law & Legal Matters', 106)");
			$q->addQuery("INSERT INTO categories VALUES (108, 'Politics', 106)");
			$q->addQuery("INSERT INTO categories VALUES (109, 'Psychology', 106)");
			$q->addQuery("INSERT INTO categories VALUES (110, 'Social Science', 106)");
			
		$q->addQuery("INSERT INTO categories VALUES (111, 'Satire', 0)");
		
	$q->execute();
	
	$link->close();
	unset($q);
			
?>
