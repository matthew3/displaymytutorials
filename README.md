# README #

### What is this repository for? ###

This is a website, with back-end, for writing custom tutorials. Using the built-in editor, users can add video, images, html and css to their own multi-part tutorials. The website will sanitize user-submitted html for security reasons, and then serve the html to other users on the website. 